<%--
  Created by IntelliJ IDEA.
  User: Matin Agahi
  Date: 2/2/2021
  Time: 9:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- FAVICON -->
    <link href="images/favicon.png" rel="shortcut icon">
    <!-- PLUGINS CSS STYLE -->
    <!-- <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"> -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap-slider.css">
    <!-- Font Awesome -->
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="plugins/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
    <!-- Fancy Box -->
    <link href="plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
    <link href="plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="css/style.css" rel="stylesheet">


</head>

<body class="body-wrapper">

<section class="login py-5 border-top-1">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-8 align-item-center">
                <div class="border border">
                    <h3 class="bg-gray p-4">Register Now</h3>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                    <fieldset class="p-4">
                        <input id="firstname" type="text" placeholder="FirstName" name="firstname" required
                               class="border p-3 w-100 my-2">
                        <input id="lastname" type="text" placeholder="LastName" name="lastname" required
                               class="border p-3 w-100 my-2">
                        <input id="email" type="email" placeholder="Email" name="email" required
                               class="border p-3 w-100 my-2">
                        <input id="username" type="text" placeholder="UserName" name="username" required
                               class="border p-3 w-100 my-2">
                        <input id="password" type="password" placeholder="Password" name="password" required
                               class="border p-3 w-100 my-2">
                        <form>
                            <div id="showthis" style="display:none">
                                <input id="companyname" type="text" placeholder="CompanyName" name="companyname"
                                       class="border p-3 w-100 my-2">
                                <input id="adress" type="text" placeholder="Address" name="adress"
                                       class="border p-3 w-100 my-2">
                                <input id="mobilenumber" type="tel" placeholder="MobileNumber" name="mobilenumber"
                                       pattern="[0-9]{11}" class="border p-3 w-100 my-2">
                            </div>
                            <input type="checkbox" id="registering" onclick="myFunction()" name="conditions"
                                   class="mt-1">
                            <label for="registering" class="px-2">I wish to be a <a
                                    class="text-primary font-weight-bold">seller</a></label>

                            <button id="register" type="submit"
                                    class="d-block py-3 px-4 bg-primary text-white border-0 rounded font-weight-bold">
                                Register
                            </button>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Successfull</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Registered Successfully</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal1" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Failed</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This user name already exist, please choose another one.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $("#register").on('click',function (e) {
        e.preventDefault();
        $.ajax({
            method: "post",
            url: "${pageContext.request.contextPath}/Register",
            data: {
                firstname: $("#firstname").val(),
                email: $("#email").val(),
                lastname: $("#lastname").val(),
                username: $("#username").val(),
                password: $("#password").val(),
                companyname: $("#companyname").val(),
                mobilenumber: $("#mobilenumber").val(),
                adress: $("#adress").val(),
                conditions: $("#registering").is(':checked'),
            },
            success: function (response) {
                if (response === "Success") {
                    $("#myModal").modal();
                } else {
                    $("#myModal1").modal();
                }
            }
        });
    });

</script>
<script>
    function myFunction() {
        var x = document.getElementById('showthis');
        if (x.style.display === 'none') {
            x.style.display = 'block';
        } else {
            x.style.display = 'none';
        }
    }
</script>
</body>

</html>
