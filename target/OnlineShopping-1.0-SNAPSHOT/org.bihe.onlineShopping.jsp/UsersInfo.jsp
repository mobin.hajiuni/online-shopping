<%@ page import="org.bihe.onlineShopping.entity.Category" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="org.bihe.onlineShopping.entity.Person" %>
<%@ page import="org.bihe.onlineShopping.enums.Role" %><%--
  Created by IntelliJ IDEA.
  User: setoo
  Date: 2/15/2021
  Time: 9:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UserInfo</title>

    <!-- FAVICON -->
    <link href="href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/images/favicon.png" rel="shortcut icon">
    <!-- PLUGINS CSS STYLE -->
    <!-- <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"> -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/css/bootstrap-slider.css">
    <!-- Font Awesome -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
    <!-- Fancy Box -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/css/style.css" rel="stylesheet">
    <style>
        .button{
            border: none;
            padding: 5px 10px;
            text-align: center;
            font-size: 16px;
            margin: 2px 1px;
            cursor: pointer;
        }
    </style>
</head>
<%
    Person logedInperson = (Person) session.getAttribute("IsLoggedIn");

%>

<body class="body-wrapper">

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light navigation">
                    <a class="navbar-brand" href="index.html">
                        <img src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/images/logo.png"
                             alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto main-nav ">
                            <li class="nav-item active">
                                <a class="nav-link"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/index.jsp">Home</a>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="">Dashboard<span><i
                                        class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Basket">Basket</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Order">Shopping
                                        Cart</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    Pages <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <%
                                    if (logedInperson != null) {
                                        if (logedInperson.getRole().equals("admin")) {

                                %>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/sellerRequest">Requests</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/users">Users</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/addCategory">Add Category</a>
                                </div>
                                <%
                                        }
                                    }
                                %>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto mt-10">
                            <li class="nav-item">
                                <a class="nav-link login-button"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/Login.jsp">Login</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>
<section class="dashboard section">
        <!-- Container Start -->
        <div class="container">

            <!-- Row Start -->
            <div class="row">
                <div class="col-md-12">
                    <div class="search-result bg-gray">
                        <h2>List Of Users</h2>
                    </div>
                </div>
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
                    <div class="widget dashboard-container my-adslist">
                        <%
                            List<Person> userList = (List<Person>) request.getAttribute("users");
                        %>
                        <%int j = 0;%>
                        <%if (userList.size() > 0) { %>
                        <table class="table table-responsive product-dashboard-table">
                            <thead>
                            <tr>
                                <th>#No</th>
                                <th>Name</th>
                                <th>UserName</th>
                                <th>&nbsp;&nbsp;Role</th>
                                <th> &nbsp;Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <% for(int i=0 ; i< userList.size(); i++) { %>
                            <% if(!userList.get(i).getRole().equals("admin") && !userList.get(i).isDeleted()) {
                                String firstName = userList.get(i).getFirstName();
                                String lastName = userList.get(i).getLastName();
                                String role = userList.get(i).getRole();
                                String uName = userList.get(i).getUserName();%>

                            <tr>
                                <td class="product-thumb">
                                    <span class="status active">&nbsp;&nbsp;&nbsp;&nbsp;<%=j+1%></span>
                                </td>
                                <td class="product-thumb">
                                    <span class="status active"> <%=firstName%> <%=lastName%></span>
                                </td>
                                <td class="product-thumb">
                                    <span class="status active">&nbsp;&nbsp;&nbsp;&nbsp;<%=uName%></span>
                                </td>
                                <td class="product-thumb">
                                    <span class="status active">&nbsp;&nbsp;&nbsp;&nbsp;  <%=role%></span>
                                </td>
                                <td class="product-details">
                                    <a class="button" href="${pageContext.request.contextPath}/userChange?id=<%=userList.get(i).getId()%>&op=edit">Edit</a>
                                    <a class="button" href="${pageContext.request.contextPath}/userChange?id=<%=userList.get(i).getId()%>&op=deny"> Delete </a>
                                </td>

                            </tr>
                            <% j ++;
                            }
                            }%>

                            </tbody>
                        </table>
                        <% }else{ %>
                        <h2>There is no user!</h2>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>

</section>

<%--<section class="section-sm">--%>
<%--    <div class="container">--%>
<%--        <div class="row">--%>
<%--            <div class="col-md-12">--%>
<%--                <div class="search-result bg-gray">--%>
<%--                    <h2>List Of Users</h2>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--        <div class="row">--%>
<%--            <div class="col-lg-9 col-md-8">--%>
<%--                <!-- ad listing list  -->--%>
<%--                <div class="ad-listing-list mt-20">--%>
<%--                    <div class="row p-lg-3 p-sm-5 p-4">--%>

<%--                        <div class="col-lg-8">--%>
<%--                            <div class="row">--%>
<%--                                <div class="col-lg-6 col-md-10">--%>
<%--                                        --%>

<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</section>--%>

<!--============================
=            Footer            =
=============================

<footer class="footer section section-sm">
     Container Start
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-7 offset-md-1 offset-lg-0">
                 About
                <div class="block about">
                     footer logo
                    <img src="images/logo-footer.png" alt="">
                     description
                    <p class="alt-color">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>
             Link list
            <div class="col-lg-2 offset-lg-1 col-md-3">
                <div class="block">
                    <h4>Site Pages</h4>
                    <ul>
                        <li><a href="#">Boston</a></li>
                        <li><a href="#">How It works</a></li>
                        <li><a href="#">Deals & Coupons</a></li>
                        <li><a href="#">Articls & Tips</a></li>
                        <li><a href="terms-condition.html">Terms & Conditions</a></li>
                    </ul>
                </div>
            </div>
             Link list
            <div class="col-lg-2 col-md-3 offset-md-1 offset-lg-0">
                <div class="block">
                    <h4>Admin Pages</h4>
                    <ul>
                        <li><a href="category.html">Category</a></li>
                        <li><a href="single.html">Single Page</a></li>
                        <li><a href="store.html">Store Single</a></li>
                        <li><a href="single-blog.html">Single Post</a>
                        </li>
                        <li><a href="blog.html">Blog</a></li>



                    </ul>
                </div>
            </div>
             Promotion
            <div class="col-lg-4 col-md-7">
                 App promotion
                <div class="block-2 app-promotion">
                    <div class="mobile d-flex">
                        <a href="">
                            Icon
                            <img src="images/footer/phone-icon.png" alt="mobile-icon">
                        </a>
                        <p>Get the Dealsy Mobile App and Save more</p>
                    </div>
                    <div class="download-btn d-flex my-3">
                        <a href="#"><img src="images/apps/google-play-store.png" class="img-fluid" alt=""></a>
                        <a href="#" class=" ml-3"><img src="images/apps/apple-app-store.png" class="img-fluid" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
-->
<!-- Container End -->
<!-- Footer Bottom
</footer>

<footer class="footer-bottom">

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-12">

            <div class="copyright">
                <p>Copyright © <script>
                    var CurrentYear = new Date().getFullYear()
                    document.write(CurrentYear)
                </script>. All Rights Reserved, theme by <a class="text-primary" href="https://themefisher.com" target="_blank">themefisher.com</a></p>
            </div>
        </div>
    </div>
</div>
<div class="top-to">
    <a id="top" class="" href="#"><i class="fa fa-angle-up"></i></a>
</div>
</footer>
-->
<!-- JAVASCRIPTS -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jQuery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/bootstrap-slider.js"></script>
<!-- tether js -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/tether/js/tether.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/raty/jquery.raty-fa.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/smoothscroll/SmoothScroll.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/google-map/gmap.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/js/script.js"></script>

</body>
</html>
