<%@ page import="org.bihe.onlineShopping.repository.OrderRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="org.bihe.onlineShopping.entity.*" %>
<%@ page import="org.bihe.onlineShopping.repository.BasketRepository" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Matin Agahi
  Date: 2/23/2021
  Time: 5:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Classimax</title>

    <!-- FAVICON -->
    <link href="images/favicon.png" rel="shortcut icon">
    <!-- PLUGINS CSS STYLE -->
    <!-- <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"> -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/css/bootstrap-slider.css">
    <!-- Font Awesome -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
    <!-- Fancy Box -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<%
    Person logedInperson = (Person) session.getAttribute("IsLoggedIn");
%>
<body class="body-wrapper">

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light navigation">
                    <a class="navbar-brand" href="index.html">
                        <img src="images/logo.png" alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto main-nav ">
                            <li class="nav-item active">
                                <a class="nav-link" href="index.html">Home</a>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="">Dashboard<span><i class="fa fa-angle-down"></i></span>
                                </a>

                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Basket">Basket</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Order">Shopping
                                        Cart</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Pages <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <%
                                    if (logedInperson != null) {
                                        if (logedInperson.getRole().equals("admin")) {

                                %>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/sellerRequest">Requests</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/users">Users</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/addCategory">Add Category</a>
                                </div>
                                <%
                                        }
                                    }
                                %>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Listing <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="category.html">Ad-Gird View</a>
                                    <a class="dropdown-item" href="ad-listing-list.html">Ad-List View</a>
                                </div>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto mt-10">
                            <li class="nav-item">
                                <a class="nav-link login-button" href="login.html">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white add-button" href="ad-listing.html"><i class="fa fa-plus-circle"></i> Add Listing</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>
<!--==================================
=            User Profile            =
===================================-->
<form action="${pageContext.request.contextPath}/Basket" method="POST">
<section class="dashboard section">
    <!-- Container Start -->
    <div class="container">
        <!-- Row Start -->
        <div class="row">
            <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
                <div class="widget dashboard-container my-adslist">
                    <%
                        //Person logenInPerson = (Person) session.getAttribute("isLoggedIn");
                        //int id = (int) logenInPerson.getId();


                        //session.setAttribute("id", id);
                        //List<BasketLine> selectedBasketLine = BasketRepository.findAllTheBaskets(1);
                        List<BasketLine> selectedBasketLine = (List<BasketLine>) request.getAttribute("basket");
                            for (BasketLine basketLine : selectedBasketLine) {
                                Merchendise merchendise = basketLine.getMerchendise();
                                String title = basketLine.getMerchendise().getTitle();
                                int quantity = basketLine.getQuantity();
                                double unitPrice = basketLine.getUnitPrice();
                                List<Album> albums = merchendise.getAlbum();
                                String image = "";
                                if (albums.size() > 0) {
                                    image = albums.get(0).getImage();
                                }
                    %>
                    <h3 class="widget-header">My Sales Report</h3>
                    <table class="table table-responsive product-dashboard-table">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Product Title</th>
                            <th class="text-center">Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="product-thumb">
                                <img width="80px" height="auto" src="<%=image%>" alt="image description"></td>
                            <td class="product-details">
                                <h3 class="title"><%=title%></h3>
                                <span class="add-id"><strong>Product ID:</strong><%=merchendise.getId()%> </span>
                                <span class="status active"><strong>Quantity</strong><%=quantity%></span>
                            </td>
                            <td class="product-category"><span class="categories"><%=quantity * unitPrice%></span></td>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <%
                        }
                    %>

                    <input type="date" placeholder="date1" name="date1" required class="border p-3 w-100 my-2">
                    <input type="date" placeholder="date2" name="date2" required class="border p-3 w-100 my-2">
                    <button type="submit" class="d-block py-3 px-4 bg-primary text-white border-0 rounded font-weight-bold">Filter</button>
                </div>


            </div>
        </div>
    </div>
</section>
</form>
<!-- JAVASCRIPTS -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jQuery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/bootstrap-slider.js"></script>
<!-- tether js -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/tether/js/tether.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/raty/jquery.raty-fa.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/smoothscroll/SmoothScroll.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/google-map/gmap.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/js/script.js"></script>

</body>

</html>
