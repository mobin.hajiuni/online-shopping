<%--
  Created by IntelliJ IDEA.
  User: Matin Agahi
  Date: 2/2/2021
  Time: 9:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<jsp:include page="/login"/> --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>login</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- FAVICON -->
    <link href="images/favicon.png" rel="shortcut icon">
    <!-- PLUGINS CSS STYLE -->
    <!-- <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"> -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap-slider.css" type="text/css">
    <!-- Font Awesome -->
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Owl Carousel -->
    <link href="plugins/slick-carousel/slick/slick.css" rel="stylesheet" type="text/css">
    <link href="plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet" type="text/css">
    <!-- Fancy Box -->
    <link href="plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet" type="text/css">
    <link href="plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet" type="text/css">
    <!-- CUSTOM CSS -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body class="body-wrapper">



<section class="login py-5 border-top-1">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-8 align-item-center">
                <div class="border">
                    <h3 class="bg-gray p-4">Login Now</h3>
                    <form action="${pageContext.request.contextPath}/login" method="post">
                        <fieldset class="p-4">
                            <input type="text" placeholder="Username" name="username" class="border p-3 w-100 my-2">
                            <input type="password" placeholder="Password" name="password" class="border p-3 w-100 my-2">

                            <button type="submit" class="d-block py-3 px-5 bg-primary text-white border-0 rounded font-weight-bold mt-3">Log in</button>
                            <a class="mt-3 d-inline-block text-primary" href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/Register.jsp">Register Now</a>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

</html>
