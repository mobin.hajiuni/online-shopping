<%@ page import="java.util.List" %>
<%@ page import="org.bihe.onlineShopping.entity.Person" %>
<%@ page import="org.bihe.onlineShopping.repository.UserRepository" %>
<%@ page import="org.bihe.onlineShopping.enums.Role" %><%--
  Created by IntelliJ IDEA.
  User: Matin Agahi
  Date: 2/24/2021
  Time: 3:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User Page</title>

    <!-- FAVICON -->
    <link href="img/favicon.png" rel="shortcut icon">
    <!-- PLUGINS CSS STYLE -->
    <!-- <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"> -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap-slider.css">
    <!-- Font Awesome -->
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="plugins/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
    <!-- Fancy Box -->
    <link href="plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
    <link href="plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<%
    //Order order = OrderRepository.findSelectedLines(1);
    List<Person> users = UserRepository.showAlltheUsers();
    session.setAttribute("users", users);
    Person logedInperson = (Person) session.getAttribute("IsLoggedIn");
%>
<body class="body-wrapper">

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light navigation">
                    <a class="navbar-brand" href="index.html">
                        <img src="images/logo.png" alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto main-nav ">
                            <li class="nav-item active">
                                <a class="nav-link" href="index.html">Home</a>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="">Dashboard<span><i class="fa fa-angle-down"></i></span>
                                </a>

                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <%
                                        if (logedInperson != null) {
                                            if (!logedInperson.getRole().equals("user")) {

                                    %>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Basket">Basket</a>
                                    <%

                                            }
                                        }
                                    %>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Order">Shopping
                                        Cart</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Pages <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <%
                                    if (logedInperson != null) {
                                        if (logedInperson.getRole().equals("admin")) {

                                %>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/sellerRequest">Requests</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/users">Users</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/addCategory">Add Category</a>
                                </div>
                                <%
                                        }
                                    }
                                %>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Listing <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="category.html">Ad-Gird View</a>
                                    <a class="dropdown-item" href="ad-listing-list.html">Ad-List View</a>
                                </div>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto mt-10">
                            <%
                                if (logedInperson == null) {
                            %>
                            <li class="nav-item">
                                <a class="nav-link login-button"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/Login.jsp">Login</a>
                            </li>
                            <%
                            } else {
                            %>
                            <li class="nav-item">
                                <a class="nav-link login-button"
                                   href="${pageContext.request.contextPath}/sign-out">Sign Out</a>
                            </li>
                            <%
                                }
                            %>
                            <%
                                if (logedInperson != null) {
                                    if (logedInperson.getRole().equals(Role.seller.toString()) && logedInperson.isAcceptance()) {
                            %>
                            <li class="nav-item">
                                <a class="nav-link text-white add-button"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/addProduct.jsp"><i
                                        class="fa fa-plus-circle"></i> Add Product</a>
                            </li>
                            <%
                                    }
                                }
                            %>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>
<!--==================================
=            User Profile            =
===================================-->
<section class="dashboard section">
    <form action="${pageContext.request.contextPath}/Order" method="POST">
        <!-- Container Start -->
        <div class="container">
            <!-- Row Start -->
            <div class="row">
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
                    <div class="widget dashboard-container my-adslist">
                        <%
                            //Person logedInPerson = (Person) session.getAttribute("isLoggedIn");
                            //float id = logedInPerson.getId();

                            if (users.size() > 0) {
                                for (Person user: users) {
                                    String firstName = user.getFirstName();
                                    String lastNAme = user.getLastName();
                                    String role = user.getRole();
                                    String uName = user.getUserName();

                        %>
                        <h3 class="widget-header">Users</h3>
                        <table class="table table-responsive product-dashboard-table">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Role</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="product-thumb">
                                    <span class="status active"><%=firstName%></span>
                                </td>
                                <td class="product-thumb">
                                    <span class="status active"><%=lastNAme%></span>
                                </td>
                                <td class="product-details">
                                <span class="status active"><%=role%></span>

                                </td>
                                <td class="product-details">
                                    <button type="submit" class="d-block py-3 px-4 bg-primary text-white border-0 rounded font-weight-bold">Edit</button>
                                </td>

                            </tr>
                            </tbody>
                        </table>
                        <%
                                }
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<!-- JAVASCRIPTS -->
<script src="plugins/jQuery/jquery.min.js"></script>
<script src="plugins/bootstrap/js/popper.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap-slider.js"></script>
<!-- tether js -->
<script src="plugins/tether/js/tether.min.js"></script>
<script src="plugins/raty/jquery.raty-fa.js"></script>
<script src="plugins/slick-carousel/slick/slick.min.js"></script>
<script src="plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="plugins/smoothscroll/SmoothScroll.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
<script src="plugins/google-map/gmap.js"></script>
<script src="js/script.js"></script>

</body>

</html>
