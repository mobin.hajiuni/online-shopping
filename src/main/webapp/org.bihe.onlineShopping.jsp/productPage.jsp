<%@ page import="java.util.List" %>
<%@ page import="org.bihe.onlineShopping.entity.Merchendise" %>
<%@ page import="org.bihe.onlineShopping.entity.Person" %>
<%@ page import="org.bihe.onlineShopping.enums.Role" %>
<%@ page import="org.bihe.onlineShopping.entity.Category" %>
<%@ page import="org.bihe.onlineShopping.repository.CategoryRepository" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: Mobin
  Date: 2/18/2021
  Time: 12:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=windows-1252" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <%
        ServletContext sc = request.getServletContext();
        List<Merchendise> merchendises = (List<Merchendise>) request.getAttribute("merchendises");
        List<Category> categories = (List<Category>) request.getAttribute("cats");
    %>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Online Shopping</title>

    <!-- FAVICON -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/images/favicon.png" rel="shortcut icon">
    <!-- PLUGINS CSS STYLE -->
    <!-- <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"> -->
    <!-- Bootstrap -->
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/css/bootstrap-slider.css">
    <!-- Font Awesome -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick-theme.css"
          rel="stylesheet">
    <!-- Fancy Box -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/fancybox/jquery.fancybox.pack.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jquery-nice-select/css/nice-select.css"
          rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<%
    Person logedInperson = (Person) session.getAttribute("IsLoggedIn");
%>
<body class="body-wrapper">

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light navigation">
                    <a class="navbar-brand" href="index.html">
                        <img src="images/logo.png" alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto main-nav ">
                            <li class="nav-item active">
                                <a class="nav-link"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/index.jsp">Home</a>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="">Dashboard<span><i
                                        class="fa fa-angle-down"></i></span>
                                </a>

                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <%
                                        if (logedInperson != null) {
                                            if (!logedInperson.getRole().equals("user")) {

                                    %>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Basket">Basket</a>
                                    <%

                                            }
                                        }
                                    %>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Order">Shopping
                                        Cart</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    Pages <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                    <%
                                        if (logedInperson != null) {
                                            if (logedInperson.getRole().equals("admin")) {

                                    %>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="${pageContext.request.contextPath}/sellerRequest">Requests</a>
                                        <a class="dropdown-item" href="${pageContext.request.contextPath}/users">Users</a>
                                        <a class="dropdown-item" href="${pageContext.request.contextPath}/addCategory">Add Category</a>
                                    </div>
                                    <%
                                            }
                                        }
                                    %>

                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    Products <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <form action="${pageContext.request.contextPath}/product-view" method="post">
                                        <input id="mainId" type="hidden" name="whichButton" value=""/>
                                        <button id="product-view" name="product-view-button" class="dropdown-item" type="submit" value="all" onclick="{document.getElementById('mainId').value = this.value}">Product View</button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto mt-10">
                            <%
                                if (logedInperson == null){
                            %>
                            <li class="nav-item">
                                <a class="nav-link login-button"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/Login.jsp">Login</a>
                            </li>
                            <%
                            }else{
                            %>
                            <li class="nav-item">
                                <a class="nav-link login-button"
                                   href="${pageContext.request.contextPath}/sign-out">Sign Out</a>
                            </li>
                            <%
                                }
                            %>
                            <%
                                if (logedInperson != null) {
                                    if (logedInperson.getRole().equals(Role.seller.toString()) && logedInperson.isAcceptance()) {
                            %>
                            <li class="nav-item">
                                <a class="nav-link text-white add-button" href="ad-listing.html"><i
                                        class="fa fa-plus-circle"></i> Add Product</a>
                            </li>
                            <%
                                    }
                                }
                            %>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>
<section class="section-sm">
    <div class="container">

        <div class="row">
            <div class="col-md-3">
                <div class="category-sidebar">
                    <div class="widget category-list">
                        <h4 class="widget-header">All Category</h4>
                        <ul class="category-list">
                            <%
                                if (categories != null) {
                                    for (Category category : categories) {
                                        List count = CategoryRepository.findCount(category.getId());
                            %>
                            <li onclick="{document.getElementById('categorySelector').value = <%=category.getName()%>}">
                                <a href="${pageContext.request.contextPath}/product-view?category=<%=category.getName()%>"><%=category.getName()%>
                                    <span><%=count.get(0).toString()%></span></a></li>
                            <%
                                    }
                                }
                            %>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-9">

                <div class="product-grid-list">
                    <div class="row mt-30">
                        <%
                            if (merchendises.size() > 0) {
                                for (Merchendise merchendise : merchendises) {
                                    int id = merchendise.getId();
                                    String img = merchendise.getAlbum().get(0).getImage();
                                    String title = merchendise.getTitle();
                                    String cat = merchendise.getCategories().get(0).getName();
                                    String summary = merchendise.getSummary().substring(0, 30) + "...";
                                    LocalDateTime date = merchendise.getCreatedAt();
                        %>
                        <div class="col-sm-12 col-lg-4 col-md-6">
                            <!-- product card -->
                            <div class="product-item bg-light">
                                <div class="card">
                                    <div class="thumb-content">
                                        <!-- <div class="price">$200</div> -->
                                        <a href="${pageContext.request.contextPath}/single-product-page?id=<%=id%>">
                                            <img class="card-img-top img-fluid" src="<%=img%>"
                                                 alt="Card image cap">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title"><%=title%>
                                        </h4>
                                        <ul class="list-inline product-meta">
                                            <li class="list-inline-item">
                                                <a href="${pageContext.request.contextPath}/product-view?category=<%=cat%>"><i class="fa fa-folder-open-o"></i><%=cat%></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a><i class="fa fa-calendar"></i><%=date.toLocalDate()%></a>
                                            </li>
                                        </ul>
                                        <p class="card-text"><%=summary%>
                                        </p>
                                        <div class="product-ratings">
                                            <ul class="list-inline">
                                                <%
                                                    for (int i = 0; i < merchendise.getAvgRating(); i++) {

                                                %>
                                                <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                                                <%
                                                    }
                                                    for (int i = 0; i < (5-merchendise.getAvgRating()); i++) {
                                                %>
                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                <%
                                                    }
                                                %>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <%
                                }
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--============================
=            Footer            =
=============================-->

<%--<footer class="footer section section-sm">--%>
<%--    <!-- Container Start -->--%>
<%--    <div class="container">--%>
<%--        <div class="row">--%>
<%--            <div class="col-lg-3 col-md-7 offset-md-1 offset-lg-0">--%>
<%--                <!-- About -->--%>
<%--                <div class="block about">--%>
<%--                    <!-- footer logo -->--%>
<%--                    <img src="images/logo-footer.png" alt="">--%>
<%--                    <!-- description -->--%>
<%--                    <p class="alt-color">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--%>
<%--                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation--%>
<%--                        ullamco--%>
<%--                        laboris nisi ut aliquip ex ea commodo consequat.</p>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <!-- Link list -->--%>
<%--            <div class="col-lg-2 offset-lg-1 col-md-3">--%>
<%--                <div class="block">--%>
<%--                    <h4>Site Pages</h4>--%>
<%--                    <ul>--%>
<%--                        <li><a href="#">Boston</a></li>--%>
<%--                        <li><a href="#">How It works</a></li>--%>
<%--                        <li><a href="#">Deals & Coupons</a></li>--%>
<%--                        <li><a href="#">Articls & Tips</a></li>--%>
<%--                        <li><a href="terms-condition.html">Terms & Conditions</a></li>--%>
<%--                    </ul>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <!-- Link list -->--%>
<%--            <div class="col-lg-2 col-md-3 offset-md-1 offset-lg-0">--%>
<%--                <div class="block">--%>
<%--                    <h4>Admin Pages</h4>--%>
<%--                    <ul>--%>
<%--                        <li><a href="category.html">Category</a></li>--%>
<%--                        <li><a href="single.html">Single Page</a></li>--%>
<%--                        <li><a href="store.html">Store Single</a></li>--%>
<%--                        <li><a href="single-blog.html">Single Post</a>--%>
<%--                        </li>--%>
<%--                        <li><a href="blog.html">Blog</a></li>--%>


<%--                    </ul>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <!-- Promotion -->--%>
<%--            <div class="col-lg-4 col-md-7">--%>
<%--                <!-- App promotion -->--%>
<%--                <div class="block-2 app-promotion">--%>
<%--                    <div class="mobile d-flex">--%>
<%--                        <a href="">--%>
<%--                            <!-- Icon -->--%>
<%--                            <img src="images/footer/phone-icon.png" alt="mobile-icon">--%>
<%--                        </a>--%>
<%--                        <p>Get the Dealsy Mobile App and Save more</p>--%>
<%--                    </div>--%>
<%--                    <div class="download-btn d-flex my-3">--%>
<%--                        <a href="#"><img src="images/apps/google-play-store.png" class="img-fluid" alt=""></a>--%>
<%--                        <a href="#" class=" ml-3"><img src="images/apps/apple-app-store.png" class="img-fluid"--%>
<%--                                                       alt=""></a>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--    <!-- Container End -->--%>
<%--</footer>--%>
<%--<!-- Footer Bottom -->--%>
<%--<footer class="footer-bottom">--%>
<%--    <!-- Container Start -->--%>
<%--&lt;%&ndash;    <div class="container">&ndash;%&gt;--%>
<%--&lt;%&ndash;        <div class="row">&ndash;%&gt;--%>
<%--&lt;%&ndash;            <div class="col-sm-6 col-12">&ndash;%&gt;--%>
<%--&lt;%&ndash;                <!-- Copyright -->&ndash;%&gt;--%>
<%--&lt;%&ndash;                <div class="copyright">&ndash;%&gt;--%>
<%--&lt;%&ndash;                    <p>Copyright �&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <script>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            var CurrentYear = new Date().getFullYear()&ndash;%&gt;--%>
<%--&lt;%&ndash;                            document.write(CurrentYear)&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </script>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        . All Rights Reserved, theme by <a class="text-primary" href="https://themefisher.com"&ndash;%&gt;--%>
<%--&lt;%&ndash;                                                           target="_blank">themefisher.com</a></p>&ndash;%&gt;--%>
<%--&lt;%&ndash;                </div>&ndash;%&gt;--%>
<%--&lt;%&ndash;            </div>&ndash;%&gt;--%>
<%--&lt;%&ndash;            <div class="col-sm-6 col-12">&ndash;%&gt;--%>
<%--&lt;%&ndash;                <!-- Social Icons -->&ndash;%&gt;--%>
<%--&lt;%&ndash;                <ul class="social-media-icons text-right">&ndash;%&gt;--%>
<%--&lt;%&ndash;                    <li><a class="fa fa-facebook" href="https://www.facebook.com/themefisher" target="_blank"></a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    <li><a class="fa fa-twitter" href="https://www.twitter.com/themefisher" target="_blank"></a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    <li><a class="fa fa-pinterest-p" href="https://www.pinterest.com/themefisher" target="_blank"></a>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    </li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    <li><a class="fa fa-vimeo" href=""></a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                </ul>&ndash;%&gt;--%>
<%--&lt;%&ndash;            </div>&ndash;%&gt;--%>
<%--&lt;%&ndash;        </div>&ndash;%&gt;--%>
<%--&lt;%&ndash;    </div>&ndash;%&gt;--%>
<%--    <!-- Container End -->--%>
<%--    <!-- To Top -->--%>
<%--    <div class="top-to">--%>
<%--        <a id="top" class="" href="#"><i class="fa fa-angle-up"></i></a>--%>
<%--    </div>--%>
<%--</footer>--%>

<!-- JAVASCRIPTS -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jQuery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/bootstrap-slider.js"></script>
<!-- tether js -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/tether/js/tether.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/raty/jquery.raty-fa.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/smoothscroll/SmoothScroll.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/google-map/gmap.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/js/script.js"></script>

</body>

</html>