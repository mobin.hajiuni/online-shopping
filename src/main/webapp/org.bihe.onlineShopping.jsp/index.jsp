<%@ page import="org.bihe.onlineShopping.entity.Person" %>
<%@ page import="org.bihe.onlineShopping.enums.Role" %>
<%@ page import="org.bihe.onlineShopping.entity.Album" %>
<%@ page import="org.bihe.onlineShopping.repository.AlbumRepository" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.bihe.onlineShopping.entity.Merchendise" %>
<%@ page import="org.bihe.onlineShopping.repository.MerchendiseRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="org.bihe.onlineShopping.entity.Category" %>
<%@ page import="org.bihe.onlineShopping.repository.CategoryRepository" %><%--
  Created by IntelliJ IDEA.
  User: setoo
  Date: 2/8/2021
  Time: 7:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=windows-1252" language="java" %>
<%--<jsp:include page="/product-view"/>--%>
<!DOCTYPE html>
<html lang="en">
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Online Shopping</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- FAVICON -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/images/favicon.png" rel="shortcut icon">
    <!-- PLUGINS CSS STYLE -->
    <!-- <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"> -->
    <!-- Bootstrap -->
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/css/bootstrap-slider.css">
    <!-- Font Awesome -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick-theme.css"
          rel="stylesheet">
    <!-- Fancy Box -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/fancybox/jquery.fancybox.pack.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jquery-nice-select/css/nice-select.css"
          rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<%
    Person logedInperson = (Person) session.getAttribute("IsLoggedIn");
%>

<body class="body-wrapper">


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light navigation">
                    <a class="navbar-brand" href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/index.jsp">
                        <img src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/images/logo.png"
                             alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto main-nav ">
                            <li class="nav-item active">
                                <a class="nav-link"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/index.jsp">Home</a>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="">Dashboard<span><i
                                        class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <%
                                        if (logedInperson != null) {
                                            if (!logedInperson.getRole().equals("user")) {

                                    %>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Basket">Basket</a>
                                    <%

                                        }
                                            }
                                %>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Order">Shopping
                                        Cart</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    Pages <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <%
                                    if (logedInperson != null) {
                                        if (logedInperson.getRole().equals("admin")) {

                                %>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/sellerRequest">Requests</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/users">Users</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/addCategory">Add Category</a>
                                </div>
                                <%
                                        }
                                    }
                                %>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    Products <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <form action="${pageContext.request.contextPath}/product-view" method="post">
                                        <button id="product-view" form="mainForm" name="product-view-button"
                                                class="dropdown-item" type="submit" value="all"
                                                onclick="{document.getElementById('mainId').value = this.value}">Product View
                                        </button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto mt-10">
                            <%
                                if (logedInperson == null) {
                            %>
                            <li class="nav-item">
                                <a class="nav-link login-button"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/Login.jsp">Login</a>
                            </li>
                            <%
                            } else {
                            %>
                            <li class="nav-item">
                                <a class="nav-link login-button"
                                   href="${pageContext.request.contextPath}/sign-out">Sign Out</a>
                            </li>
                            <%
                                }
                            %>
                            <%
                                if (logedInperson != null) {
                                    if (logedInperson.getRole().equals(Role.seller.toString()) && logedInperson.isAcceptance()) {
                            %>
                            <li class="nav-item">
                                <a class="nav-link text-white add-button"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/addProduct.jsp"><i
                                        class="fa fa-plus-circle"></i> Add Product</a>
                            </li>
                            <%
                                    }
                                }
                            %>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>

<!--===============================
=            Hero Area            =
================================-->
<%
    Category cat1 = CategoryRepository.findById(1);
    Category cat2 = CategoryRepository.findById(2);
%>

<section class="hero-area bg-1 text-center overly">
    <!-- Container Start -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Header Contetnt -->
                <div class="content-block">
                    <h1>Buy & Sell Near You </h1>
                    <p>Welcome to new generation of sales<br> Join the millions who buy and sell from each other</p>
                    <div class="short-popular-category-list text-center">
                        <h2>Popular Category</h2>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <form action="${pageContext.request.contextPath}/product-view" method="post">
                                    <button id="btn1" form="mainForm" type="submit" value="<%=cat2.getId()%>"
                                            name="Electronics"
                                            onclick="{document.getElementById('mainId').value = this.value}"><i
                                            class="fa fa-grav"></i><%=cat2.getName()%>
                                    </button>
                                </form>
                            </li>
                            <li class="list-inline-item">
                                <form id="mainForm" action="${pageContext.request.contextPath}/product-view"
                                      method="post">
                                    <input id="mainId" type="hidden" name="whichButton" value=""/>
                                    <button id="btn2" type="submit" value="<%=cat1.getId()%>" name="LapTops"
                                            onclick="{document.getElementById('mainId').value = this.value}"><i
                                            class="fa fa-grav"></i><%=cat1.getName()%>
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- Advance Search -->
                <div class="advance-search">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-12 col-md-12 align-content-center">
                                <form action="" method="post">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <input form="mainForm" name="productName" type="text"
                                                   class="form-control my-2 my-lg-1" id="inputtext4"
                                                   placeholder="What are you looking for"/>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <select form="mainForm" name="categorySelect"
                                                    class="w-100 form-control mt-lg-1 mt-md-2">
                                                <option name="select">Select Category</option>
                                                <%
                                                    List<Category> categories = CategoryRepository.findAll();
                                                    for (Category category :
                                                            categories) {
                                                %>
                                                <option name="categorySelect"><%= category.getName()%>
                                                </option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2 align-self-center">
                                            <button type="submit" form="mainForm" class="btn btn-primary" value="search"
                                                    onclick="{document.getElementById('mainId').value = this.value}">
                                                Search Now
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container End -->
</section>

<!--===================================
=            Client Slider            =
====================================-->


<!--===========================================
=            Popular deals section            =
============================================-->

<section class="popular-deals section bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2></h2>
                    <p>Random Recent Products</p>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- offer 01 -->
            <div class="col-lg-12">
                <div class="trending-ads-slide">
                    <%
                        List<Merchendise> selectedMerchendises = MerchendiseRepository.findSelectedItems();

                        if (selectedMerchendises.size() > 0) {
                            for (Merchendise merchendise : selectedMerchendises) {
                                int id = merchendise.getId();
                                String title = merchendise.getTitle();
                                String cat = merchendise.getCategories().get(0).getName();
                                String summary = merchendise.getSummary().substring(0, 100) + "...";
                                List<Album> albums = merchendise.getAlbum();
                                String image = "";
                                if (albums.size() > 0) {
                                    image = albums.get(0).getImage();
                                }
                    %>
                    <div class="col-sm-12 col-lg-4">
                        <!-- product card -->
                        <div class="product-item bg-light">
                            <div class="card">
                                <div class="thumb-content">
                                    <!-- <div class="price">$200</div> -->
                                    <a href="${pageContext.request.contextPath}/single-product-page?id=<%=id%>">
                                        <img class="card-img-top img-fluid"
                                             src="<%=image%>"
                                             alt="Card image cap">
                                    </a>
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title"><a href="single.html"><%=title%>
                                    </a></h4>
                                    <ul class="list-inline product-meta">
                                        <li class="list-inline-item">
                                            <a href="${pageContext.request.contextPath}/product-view?category=<%=cat%>"><i class="fa fa-folder-open-o"></i><%=cat%>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="#"><i class="fa fa-calendar"></i>26th December</a>
                                        </li>
                                    </ul>
                                    <p class="card-text"><%=summary%>
                                    </p>
                                    <div class="product-ratings">
                                        <ul class="list-inline">
                                            <%
                                                for (int i = 0; i < merchendise.getAvgRating(); i++) {

                                            %>
                                            <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                                            <%
                                                }
                                                for (int i = 0; i < (5-merchendise.getAvgRating()); i++) {
                                            %>
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <%
                                                }
                                            %>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <%
                            }
                        }
                    %>
                </div>
            </div>


        </div>
    </div>
</section>


<!--==========================================
=            All Category Section            =
===========================================-->

<%--<section class=" section">--%>
<%--    <!-- Container Start -->--%>
<%--    <div class="container">--%>
<%--        <div class="row">--%>
<%--            <div class="col-12">--%>
<%--                <!-- Section title -->--%>
<%--                <div class="section-title">--%>
<%--                    <h2>All Categories</h2>--%>
<%--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, provident!</p>--%>
<%--                </div>--%>
<%--                <div class="row">--%>
<%--                    <!-- Category list -->--%>
<%--                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">--%>
<%--                        <div class="category-block">--%>
<%--                            <div class="header">--%>
<%--                                <i class="fa fa-laptop icon-bg-1"></i>--%>
<%--                                <h4>Electronics</h4>--%>
<%--                            </div>--%>
<%--                            <ul class="category-list">--%>
<%--                                <li><a href="category.html">Laptops <span>93</span></a></li>--%>
<%--                                <li><a href="category.html">Iphone <span>233</span></a></li>--%>
<%--                                <li><a href="category.html">Microsoft <span>183</span></a></li>--%>
<%--                                <li><a href="category.html">Monitors <span>343</span></a></li>--%>
<%--                            </ul>--%>
<%--                        </div>--%>
<%--                    </div> <!-- /Category List -->--%>
<%--                    <!-- Category list -->--%>
<%--                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">--%>
<%--                        <div class="category-block">--%>
<%--                            <div class="header">--%>
<%--                                <i class="fa fa-apple icon-bg-2"></i>--%>
<%--                                <h4>Restaurants</h4>--%>
<%--                            </div>--%>
<%--                            <ul class="category-list">--%>
<%--                                <li><a href="category.html">Cafe <span>393</span></a></li>--%>
<%--                                <li><a href="category.html">Fast food <span>23</span></a></li>--%>
<%--                                <li><a href="category.html">Restaurants <span>13</span></a></li>--%>
<%--                                <li><a href="category.html">Food Track<span>43</span></a></li>--%>
<%--                            </ul>--%>
<%--                        </div>--%>
<%--                    </div> <!-- /Category List -->--%>
<%--                    <!-- Category list -->--%>
<%--                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">--%>
<%--                        <div class="category-block">--%>
<%--                            <div class="header">--%>
<%--                                <i class="fa fa-home icon-bg-3"></i>--%>
<%--                                <h4>Real Estate</h4>--%>
<%--                            </div>--%>
<%--                            <ul class="category-list">--%>
<%--                                <li><a href="category.html">Farms <span>93</span></a></li>--%>
<%--                                <li><a href="category.html">Gym <span>23</span></a></li>--%>
<%--                                <li><a href="category.html">Hospitals <span>83</span></a></li>--%>
<%--                                <li><a href="category.html">Parolurs <span>33</span></a></li>--%>
<%--                            </ul>--%>
<%--                        </div>--%>
<%--                    </div> <!-- /Category List -->--%>
<%--                    <!-- Category list -->--%>
<%--                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">--%>
<%--                        <div class="category-block">--%>
<%--                            <div class="header">--%>
<%--                                <i class="fa fa-shopping-basket icon-bg-4"></i>--%>
<%--                                <h4>Shoppings</h4>--%>
<%--                            </div>--%>
<%--                            <ul class="category-list">--%>
<%--                                <li><a href="category.html">Mens Wears <span>53</span></a></li>--%>
<%--                                <li><a href="category.html">Accessories <span>212</span></a></li>--%>
<%--                                <li><a href="category.html">Kids Wears <span>133</span></a></li>--%>
<%--                                <li><a href="category.html">It & Software <span>143</span></a></li>--%>
<%--                            </ul>--%>
<%--                        </div>--%>
<%--                    </div> <!-- /Category List -->--%>
<%--                    <!-- Category list -->--%>
<%--                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">--%>
<%--                        <div class="category-block">--%>
<%--                            <div class="header">--%>
<%--                                <i class="fa fa-briefcase icon-bg-5"></i>--%>
<%--                                <h4>Jobs</h4>--%>
<%--                            </div>--%>
<%--                            <ul class="category-list">--%>
<%--                                <li><a href="category.html">It Jobs <span>93</span></a></li>--%>
<%--                                <li><a href="category.html">Cleaning & Washing <span>233</span></a></li>--%>
<%--                                <li><a href="category.html">Management <span>183</span></a></li>--%>
<%--                                <li><a href="category.html">Voluntary Works <span>343</span></a></li>--%>
<%--                            </ul>--%>
<%--                        </div>--%>
<%--                    </div> <!-- /Category List -->--%>
<%--                    <!-- Category list -->--%>
<%--                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">--%>
<%--                        <div class="category-block">--%>
<%--                            <div class="header">--%>
<%--                                <i class="fa fa-car icon-bg-6"></i>--%>
<%--                                <h4>Vehicles</h4>--%>
<%--                            </div>--%>
<%--                            <ul class="category-list">--%>
<%--                                <li><a href="category.html">Bus <span>193</span></a></li>--%>
<%--                                <li><a href="category.html">Cars <span>23</span></a></li>--%>
<%--                                <li><a href="category.html">Motobike <span>33</span></a></li>--%>
<%--                                <li><a href="category.html">Rent a car <span>73</span></a></li>--%>
<%--                            </ul>--%>
<%--                        </div>--%>
<%--                    </div> <!-- /Category List -->--%>
<%--                    <!-- Category list -->--%>
<%--                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">--%>
<%--                        <div class="category-block">--%>
<%--                            <div class="header">--%>
<%--                                <i class="fa fa-paw icon-bg-7"></i>--%>
<%--                                <h4>Pets</h4>--%>
<%--                            </div>--%>
<%--                            <ul class="category-list">--%>
<%--                                <li><a href="category.html">Cats <span>65</span></a></li>--%>
<%--                                <li><a href="category.html">Dogs <span>23</span></a></li>--%>
<%--                                <li><a href="category.html">Birds <span>113</span></a></li>--%>
<%--                                <li><a href="category.html">Others <span>43</span></a></li>--%>
<%--                            </ul>--%>
<%--                        </div>--%>
<%--                    </div> <!-- /Category List -->--%>
<%--                    <!-- Category list -->--%>
<%--                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">--%>
<%--                        <div class="category-block">--%>

<%--                            <div class="header">--%>
<%--                                <i class="fa fa-laptop icon-bg-8"></i>--%>
<%--                                <h4>Services</h4>--%>
<%--                            </div>--%>
<%--                            <ul class="category-list">--%>
<%--                                <li><a href="category.html">Cleaning <span>93</span></a></li>--%>
<%--                                <li><a href="category.html">Car Washing <span>233</span></a></li>--%>
<%--                                <li><a href="category.html">Clothing <span>183</span></a></li>--%>
<%--                                <li><a href="category.html">Business <span>343</span></a></li>--%>
<%--                            </ul>--%>
<%--                        </div>--%>
<%--                    </div> <!-- /Category List -->--%>


<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--    <!-- Container End -->--%>
<%--</section>--%>


<!--====================================
=            Call to Action            =
=====================================-->

<%--<section class="call-to-action overly bg-3 section-sm">--%>
<%--    <!-- Container Start -->--%>
<%--    <div class="container">--%>
<%--        <div class="row justify-content-md-center text-center">--%>
<%--            <div class="col-md-8">--%>
<%--                <div class="content-holder">--%>
<%--                    <h2>Start today to get more exposure and--%>
<%--                        grow your business</h2>--%>
<%--                    <ul class="list-inline mt-30">--%>
<%--                        <li class="list-inline-item"><a class="btn btn-main" href="ad-listing.html">Add Listing</a></li>--%>
<%--                        <li class="list-inline-item"><a class="btn btn-secondary" href="category.html">Browser--%>
<%--                            Listing</a></li>--%>
<%--                    </ul>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--    <!-- Container End -->--%>
<%--</section>--%>

<!--============================
=            Footer            =
=============================-->

<%--<footer class="footer section section-sm">--%>
<%--    <!-- Container Start -->--%>
<%--    <div class="container">--%>
<%--        <div class="row">--%>
<%--            <div class="col-lg-3 col-md-7 offset-md-1 offset-lg-0">--%>
<%--                <!-- About -->--%>
<%--                <div class="block about">--%>
<%--                    <!-- footer logo -->--%>
<%--                    <img src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/images/logo-footer.png"--%>
<%--                         alt="">--%>
<%--                    <!-- description -->--%>
<%--                    <p class="alt-color">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--%>
<%--                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation--%>
<%--                        ullamco--%>
<%--                        laboris nisi ut aliquip ex ea commodo consequat.</p>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <!-- Link list -->--%>
<%--            <div class="col-lg-2 offset-lg-1 col-md-3">--%>
<%--                <div class="block">--%>
<%--                    <h4>Site Pages</h4>--%>
<%--                    <ul>--%>
<%--                        <li><a href="#">Boston</a></li>--%>
<%--                        <li><a href="#">How It works</a></li>--%>
<%--                        <li><a href="#">Deals & Coupons</a></li>--%>
<%--                        <li><a href="#">Articls & Tips</a></li>--%>
<%--                        <li><a href="terms-condition.html">Terms & Conditions</a></li>--%>
<%--                    </ul>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <!-- Link list -->--%>
<%--            <div class="col-lg-2 col-md-3 offset-md-1 offset-lg-0">--%>
<%--                <div class="block">--%>
<%--                    <h4>Admin Pages</h4>--%>
<%--                    <ul>--%>
<%--                        <li><a href="category.html">Category</a></li>--%>
<%--                        <li><a href="single.html">Single Page</a></li>--%>
<%--                        <li><a href="store.html">Store Single</a></li>--%>
<%--                        <li><a href="single-blog.html">Single Post</a>--%>
<%--                        </li>--%>
<%--                        <li><a href="blog.html">Blog</a></li>--%>


<%--                    </ul>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <!-- Promotion -->--%>
<%--            <div class="col-lg-4 col-md-7">--%>
<%--                <!-- App promotion -->--%>
<%--                <div class="block-2 app-promotion">--%>
<%--                    <div class="mobile d-flex">--%>
<%--                        <a href="">--%>
<%--                            <!-- Icon -->--%>
<%--                            <img src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/images/footer/phone-icon.png"--%>
<%--                                 alt="mobile-icon">--%>
<%--                        </a>--%>
<%--                        <p>Get the Dealsy Mobile App and Save more</p>--%>
<%--                    </div>--%>
<%--                    <div class="download-btn d-flex my-3">--%>
<%--                        <a href="#"><img--%>
<%--                                src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/images/apps/google-play-store.png"--%>
<%--                                class="img-fluid" alt=""></a>--%>
<%--                        <a href="#" class=" ml-3"><img--%>
<%--                                src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/images/apps/apple-app-store.png"--%>
<%--                                class="img-fluid"--%>
<%--                                alt=""></a>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--    <!-- Container End -->--%>
<%--</footer>--%>
<%--<!-- Footer Bottom -->--%>
<%--<footer class="footer-bottom">--%>
<%--    <!-- Container Start -->--%>
<%--    <div class="container">--%>
<%--        <div class="row">--%>
<%--            <div class="col-sm-6 col-12">--%>
<%--                <!-- Copyright -->--%>
<%--                <div class="copyright">--%>
<%--                    <p>Copyright �--%>
<%--                        <script>--%>
<%--                            var CurrentYear = new Date().getFullYear()--%>
<%--                            document.write(CurrentYear)--%>
<%--                        </script>--%>
<%--                        . All Rights Reserved, theme by <a class="text-primary" href="https://themefisher.com"--%>
<%--                                                           target="_blank">themefisher.com</a></p>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <div class="col-sm-6 col-12">--%>
<%--                <!-- Social Icons -->--%>
<%--                <ul class="social-media-icons text-right">--%>
<%--                    <li><a class="fa fa-facebook" href="https://www.facebook.com/themefisher" target="_blank"></a></li>--%>
<%--                    <li><a class="fa fa-twitter" href="https://www.twitter.com/themefisher" target="_blank"></a></li>--%>
<%--                    <li><a class="fa fa-pinterest-p" href="https://www.pinterest.com/themefisher" target="_blank"></a>--%>
<%--                    </li>--%>
<%--                    <li><a class="fa fa-vimeo" href=""></a></li>--%>
<%--                </ul>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--    <!-- Container End -->--%>
<%--    <!-- To Top -->--%>
<%--    <div class="top-to">--%>
<%--        <a id="top" class="" href="#"><i class="fa fa-angle-up"></i></a>--%>
<%--    </div>--%>
<%--</footer>--%>

<!-- JAVASCRIPTS -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jQuery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/bootstrap-slider.js"></script>
<!-- tether js -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/tether/js/tether.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/raty/jquery.raty-fa.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/smoothscroll/SmoothScroll.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/google-map/gmap.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/js/script.js"></script>

</body>

</html>




