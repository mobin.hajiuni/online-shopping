<%@ page import="java.util.List" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="org.bihe.onlineShopping.repository.RatingRepository" %>
<%@ page import="org.bihe.onlineShopping.entity.*" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.Clock" %>
<%@ page import="org.bihe.onlineShopping.enums.Role" %><%--
  Created by IntelliJ IDEA.
  User: Mobin
  Date: 2/21/2021
  Time: 9:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=windows-1252" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <%
        Person loggedinPerson = (Person) session.getAttribute("OnlineUser");
        Merchendise merchendise = (Merchendise) request.getAttribute("merchendise");
    %>
    <script src="http://code.jquery.com/jquery-3.4.1.js"></script>
    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product Page</title>

    <!-- FAVICON -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/images/favicon.png" rel="shortcut icon">
    <!-- PLUGINS CSS STYLE -->
    <!-- <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"> -->
    <!-- Bootstrap -->
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/css/bootstrap-slider.css">
    <!-- Font Awesome -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick-theme.css"
          rel="stylesheet">
    <!-- Fancy Box -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/fancybox/jquery.fancybox.pack.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jquery-nice-select/css/nice-select.css"
          rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<%
    Person logedInperson = (Person) session.getAttribute("IsLoggedIn");
%>
<body class="body-wrapper">

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light navigation">
                    <a class="navbar-brand" href="index.html">
                        <img src="images/logo.png" alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto main-nav ">
                            <li class="nav-item active">
                                <a class="nav-link"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/index.jsp">Home</a>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="">Dashboard<span><i
                                        class="fa fa-angle-down"></i></span>
                                </a>

                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <%
                                        if (logedInperson != null) {
                                            if (!logedInperson.getRole().equals("user")) {

                                    %>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Basket">Basket</a>
                                    <%

                                            }
                                        }
                                    %>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/Order">Shopping
                                        Cart</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    Pages <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <%
                                    if (logedInperson != null) {
                                        if (logedInperson.getRole().equals("admin")) {

                                %>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/sellerRequest">Requests</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/users">Users</a>
                                    <a class="dropdown-item" href="${pageContext.request.contextPath}/addCategory">Add
                                        Category</a>
                                </div>
                                <%
                                        }
                                    }
                                %>
                            </li>
                            <li class="nav-item dropdown dropdown-slide">
                                <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    Products <span><i class="fa fa-angle-down"></i></span>
                                </a>
                                <!-- Dropdown list -->
                                <div class="dropdown-menu">
                                    <a class="dropdown-item"
                                       href="${pageContext.request.contextPath}/product-view?category=all">Product View</a>
                                </div>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto mt-10">
                            <%
                                if (loggedinPerson == null) {
                            %>
                            <li class="nav-item">
                                <a class="nav-link login-button"
                                   href="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/Login.jsp">Login</a>
                            </li>
                            <%
                            } else {
                            %>
                            <li class="nav-item">
                                <a class="nav-link login-button"
                                   href="${pageContext.request.contextPath}/sign-out">Sign Out</a>
                            </li>
                            <%
                                }
                            %>
                            <%
                                if (loggedinPerson != null) {
                                    if (loggedinPerson.getRole().equals(Role.seller.toString())) {
                            %>
                            <li class="nav-item">
                                <a class="nav-link text-white add-button" href="ad-listing.html"><i
                                        class="fa fa-plus-circle"></i> Add Product</a>
                            </li>
                            <%
                                    }
                                }
                            %>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>
<section class="page-search">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
    </div>
</section>
<!--===================================
=            Store Section            =
====================================-->
<section class="section bg-gray">
    <!-- Container Start -->
    <div class="container">
        <div class="row">
            <!-- Left sidebar -->
            <div class="col-md-8">
                <div class="product-details">
                    <h1 class="product-title"><%=merchendise.getTitle()%>
                    </h1>
                    <div class="product-meta">
                        <ul class="list-inline">
                            <li class="list-inline-item"><i class="fa fa-folder-open-o"></i> Category<a
                                    href="${pageContext.request.contextPath}/product-view?category=<%=merchendise.getCategories().get(0).getName()%>"><%=merchendise.getCategories().get(0).getName()%>
                            </a></li>
                        </ul>
                    </div>

                    <!-- product slider -->
                    <div class="product-slider">
                        <%
                            List<Album> albums = merchendise.getAlbum();

                            if (albums.size() > 0) {
                                for (Album album : albums) {
                                    String img = album.getImage();

                        %>
                        <div class="product-slider-item my-4">
                            <img class="img-fluid w-100" src="<%=img%>">
                        </div>
                        <%
                                }
                            }
                        %>
                    </div>
                    <!-- product slider -->

                    <div class="content mt-5 pt-5">
                        <ul class="nav nav-pills  justify-content-center" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                                   role="tab" aria-controls="pills-home"
                                   aria-selected="true">Product Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                                   role="tab" aria-controls="pills-profile"
                                   aria-selected="false">Specifications</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                                   role="tab" aria-controls="pills-contact"
                                   aria-selected="false">Reviews</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                 aria-labelledby="pills-home-tab">
                                <h3 class="tab-title">Product Description</h3>
                                <p><%=merchendise.getSummary()%>
                                </p>
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                 aria-labelledby="pills-profile-tab">
                                <h3 class="tab-title">Product Specifications</h3>
                                <table class="table table-bordered product-table">
                                    <tbody>
                                    <%
                                        String brand = merchendise.getBrand();
                                        String color = merchendise.getColor();
                                        int Quantity = merchendise.getQuantity();
                                        Person person = merchendise.getPerson();
                                        List<Comment> comments = merchendise.getComments();
                                        int avgRating = merchendise.getAvgRating();
                                    %>
                                    <tr>
                                        <td>Brand</td>
                                        <td><%=brand%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Color</td>
                                        <td><%=color%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Quantity</td>
                                        <td><%=Quantity%>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                                 aria-labelledby="pills-contact-tab">
                                <h3 class="tab-title">Product Review</h3>
                                <div class="product-review">
                                    <div class="media">
                                        <!-- Avater -->
                                        <div class="media-body" id="media-body">
                                            <!-- Ratings -->
                                            <%
                                                if (comments != null && comments.size() > 0) {
                                                    for (Comment comment : comments) {
                                                        Person commentPerson = comment.getPerson();
                                                        LocalDateTime createdAt = comment.getCreatedAt();
                                                        int rating = 0;
                                                        try {
                                                            rating = RatingRepository.findByPersonAndMerchendise(comment.getPerson().getId(), merchendise.getId());
                                                        } catch (Exception e) {
                                                            System.out.println("no rating");
                                                        }
                                            %>
                                            <div class="ratings">
                                                <ul class="list-inline">
                                                    <%
                                                        if (rating != 0) {
                                                            for (int i = 0; i < rating; i++) {

                                                    %>
                                                    <li class="list-inline-item">
                                                        <i class="fa fa-star"></i>
                                                    </li>
                                                    <%
                                                        }
                                                        for (int i = 0; i < 5 - rating; i++) {
                                                    %>
                                                    <li class="list-inline-item">
                                                        <i class="fa fa-star-o"></i>
                                                    </li>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </ul>
                                            </div>
                                            <div class="name">
                                                <h5><%=commentPerson.getFirstName() + " " + commentPerson.getLastName()%>
                                                </h5>
                                            </div>
                                            <div class="date">
                                                <p><%=createdAt.toLocalDate()%>
                                                </p>
                                            </div>
                                            <div class="review-comment">
                                                <p><%=comment.getComment()%>
                                                </p>
                                            </div>
                                            <%
                                                    }
                                                }
                                            %>
                                        </div>
                                    </div>
                                    <%
                                        if (loggedinPerson != null) {
                                    %>
                                    <div class="review-submission">
                                        <h3 class="tab-title">Submit your review</h3>
                                        <!-- Rate -->
                                        <div class="rate">
                                            <div class="starrr" id="star"></div>
                                        </div>
                                        <div class="review-submit">
                                            <form action="#" class="row">
                                                <%--                                                <div class="col-lg-6">--%>
                                                <%--                                                    <input type="text" name="name" id="name" class="form-control"--%>
                                                <%--                                                           placeholder="Name">--%>
                                                <%--                                                </div>--%>
                                                <%--                                                <div class="col-lg-6">--%>
                                                <%--                                                    <input type="email" name="email" id="email" class="form-control"--%>
                                                <%--                                                           placeholder="Email">--%>
                                                <%--                                                </div>--%>
                                                <div class="col-12">
                                                    <textarea name="review" id="review" rows="10" class="form-control"
                                                              placeholder="Message"></textarea>
                                                </div>
                                                <div class="col-12">
                                                    <button id="reviewButton" type="button" class="btn btn-main"
                                                            value="hi">Sumbit
                                                    </button>

                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <%
                                    } else {
                                    %>
                                    <div class="review-submission">
                                        <h3 class="tab-title">You Need To Log in to Post a Review</h3>
                                    </div>
                                    <%
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sidebar">
                    <div class="widget price text-center">
                        <h4>Price</h4>
                        <p><%=merchendise.getPrice() + " USD"%>
                        </p>
                    </div>
                    <!-- User Profile widget -->
                    <div class="widget user text-center">
                        <li class="list-inline-item">
                            <button id="AddToCart"
                                    class="btn btn-contact d-inline-block  btn-primary px-lg-5 my-1 px-md-3">Add
                                to My Cart
                            </button>
                        </li>
                        </ul>
                    </div>
                    <%
                        if (avgRating != 0) {
                    %>
                    <div class="widget user text-center">
                        <h5 class="widget-header text-center">Product's Rate</h5>
                        <ul class="list-inline">
                            <%
                                for (int i = 0; i < avgRating; i++) {
                            %>
                            <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                            <%
                                }
                                for (int i = 0; i < (5 - avgRating); i++) {
                            %>
                            <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                            <%
                                }
                            %>
                        </ul>
                    </div>

                    <%
                        }
                    %>

                </div>
            </div>

        </div>
    </div>
    <!-- Container End -->
    <!-- Modal -->
    <div id="myModal1" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Successfull</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Successfully added</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal2" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Sign in Needed</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>You need to sign in first</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

</section>
<%
    if (loggedinPerson != null) {
%>
<script>
    $("#reviewButton").on('click', function () {
        $.ajax({
            method: "post",
            url: "${pageContext.request.contextPath}/save-comment",
            data: {
                comment: $('#review').val(),
                name: "comment",
                personId: "<%=loggedinPerson.getId()%>",
                merchendiseId: "<%=merchendise.getId()%>",
                stars: $('#star .fa-star').length,
            },
            success: function (response) {
                if (response === "Success") {
                    $("#media-body").append("<div class=\"ratings\">\n" +
                        "                                                <ul class=\"list-inline\">\n" +
                        "                                                    \n" +
                        "                                                    <li class=\"list-inline-item\">\n" +
                        "                                                        <i class=\"fa fa-star\"></i>\n" +
                        "                                                    </li>\n" +
                        "                                                \n" +
                        "                                                                                                      <li class=\"list-inline-item\">\n" +
                        "                                                                                                          <i class=\"fa fa-star\"></i>\n" +
                        "                                                                                                      </li>\n" +
                        "                                                                                                      <li class=\"list-inline-item\">\n" +
                        "                                                                                                            <i class=\"fa fa-star\"></i>\n" +
                        "                                                                                                        </li>\n" +
                        "                                                                                                        <li class=\"list-inline-item\">\n" +
                        "                                                                                                            <i class=\"fa fa-star\"></i>\n" +
                        "                                                                                                        </li>\n" +
                        "                                                                                                        <li class=\"list-inline-item\">\n" +
                        "                                                                                                            <i class=\"fa fa-star\"></i>\n" +
                        "                                                                                                        </li>\n" +
                        "                                                </ul>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"name\">\n" +
                        "                                                <h5><%=loggedinPerson.getFirstName() +" "+loggedinPerson.getLastName()%>\n" +
                        "                                                </h5>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"date\">\n" +
                        "                                                <p><%Clock clock = Clock.systemUTC(); LocalDate l = LocalDate.now(clock);%> <%=l%>\n" +
                        "                                                </p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"review-comment\">\n" +
                        "                                                <p>" + $('#review').val() +
                        "                                                </p>\n" +
                        "                                            </div>")
                }
            }
        });
    });


</script>
<script>
    $("#AddToCart").on('click', function () {
        $.ajax({
            method: "post",
            url: "${pageContext.request.contextPath}/add-to-cart",
            data: {
                personId: "<%=loggedinPerson.getId()%>",
                merchendiseId: "<%=merchendise.getId()%>",
            },
            success: function (response) {
                if (response === "Success") {
                    $("#myModal1").modal();
                } else {

                }
            }

        });
    });
</script>
<%
} else {
%>
<script>
    $("#AddToCart").on('click', function () {
        $('#myModal2').modal();
    });
</script>
<%
    }
%>

<!-- JAVASCRIPTS -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jQuery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/bootstrap/js/bootstrap-slider.js"></script>
<!-- tether js -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/tether/js/tether.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/raty/jquery.raty-fa.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/slick-carousel/slick/slick.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/plugins/smoothscroll/SmoothScroll.min.js"></script>
<!-- google map -->
<script src="${pageContext.request.contextPath}/org.bihe.onlineShopping.jsp/js/script.js"></script>

</body>

</html>