package org.bihe.onlineShopping.repository;

import org.bihe.onlineShopping.entity.Merchendise;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Insert {

    public static void insertData(Object object) {

        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

//        session.persist(object);
         session.save(object);
        transaction.commit();

        session.close();

    }

    public static void updateData(Object object) {

        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

//        session.persist(object);
        session.update(object);
        transaction.commit();

        session.close();

    }

}
