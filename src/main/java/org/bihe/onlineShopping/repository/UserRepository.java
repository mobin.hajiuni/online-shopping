package org.bihe.onlineShopping.repository;

import org.bihe.onlineShopping.entity.Order;
import org.bihe.onlineShopping.entity.Person;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.List;

public class UserRepository {
    public static List<Person> showAlltheUsers(){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = " from Person";
        Query query = session.createQuery(hql);
        List<Person> result = query.getResultList();


        session.close();

        return result;
    }
}
