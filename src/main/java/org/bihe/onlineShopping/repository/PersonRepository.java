package org.bihe.onlineShopping.repository;

import org.bihe.onlineShopping.entity.Person;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import javax.persistence.Id;
import javax.persistence.Query;
import java.util.List;

public class PersonRepository {

    public static List findByUserName(String username) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();
        String hql = "from Person where userName like :userNamei";
        Query query = session.createQuery(hql);
        query.setParameter("userNamei",username);
        List result = query.getResultList();
        session.close();
        return result;
    }

    public static List readPersons() {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();
        String hql = "from Person";
        Query query = session.createQuery(hql);
        List result = query.getResultList();
        session.close();
        return result;
    }
    public static List findByAcceptance(boolean acceptance) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();
        String hql = "from Person where acceptance =:acc and deleted=false and (role like :us or role like :sel)";
        Query query = session.createQuery(hql);
        query.setParameter("acc",acceptance);
        query.setParameter("us","user");
        query.setParameter("sel","seller");
        List result = query.getResultList();
        session.close();
        return result;
    }
    public static List findByID(int Id) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from Person where id =:ID";
        Query query = session.createQuery(hql);
        query.setParameter("ID",Id);
        List result = query.getResultList();
        session.close();
        return result;
    }
    public static void accept(int ID) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();
        String hql = "update Person set Person.acceptance=true, Person.role='seller' where id =:idi";
        Query query = session.createQuery(hql);
        query.setParameter("idi",ID);
        query.executeUpdate();
        session.close();
    }

    public static void deny(int ID) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();
        String hql = "DELETE from Person where id =: idi";
        Query query = session.createQuery(hql);
        query.setParameter("idi",ID);
        query.executeUpdate();
        session.close();
    }
    public static void changeRoll(int ID, String ro) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();
        String hql = "update Person set Person.role=:rolli where id =:idi";
        Query query = session.createQuery(hql);
        query.setParameter("idi",ID);
        query.setParameter("rolli",ro);
        query.executeUpdate();
        session.close();
    }
    public static Person findByUserId(int ID) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from Person where id = :id";

        Query query = session.createQuery(hql);
        query.setParameter("id", ID);
        List result = query.getResultList();

        session.close();
        Person person = null;
        if (result!= null && result.size()>0){
            person = (Person) result.get(0);
        }
        return person;
    }
}
