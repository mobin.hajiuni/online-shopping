package org.bihe.onlineShopping.repository;

import org.bihe.onlineShopping.entity.Album;
import org.bihe.onlineShopping.entity.Merchendise;
import org.bihe.onlineShopping.entity.Order;
import org.bihe.onlineShopping.entity.OrderLine;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.security.PublicKey;
import java.util.List;

public class OrderRepository {
    public static Order findSelectedLines(int id){
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            Session session = configuration.buildSessionFactory().openSession();

            String hql = " from Order o where person.id=:idd";
            Query query = session.createQuery(hql);
            query.setParameter("idd", id);
            query.setMaxResults(1);
            List<Order> result = query.getResultList();
            try {
                    Order or = result.get(0);
                    session.close();
                    return or;
            }catch (Exception e){

                    session.close();
                    return null;
            }


    }

        public static List<Order> findActiveOrders(int personId){
                Configuration configuration = new Configuration();
                configuration.configure("hibernate.cfg.xml");
                Session session = configuration.buildSessionFactory().openSession();

                String hql = "from Order o where person.id=:id and status = true ";
                Query query = session.createQuery(hql);
                query.setParameter("id", personId);
                query.setMaxResults(1);
                List<Order> result = query.getResultList();

                session.close();

                return result;

        }

}
