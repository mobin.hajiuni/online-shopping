package org.bihe.onlineShopping.repository;

import org.bihe.onlineShopping.entity.Merchendise;
import org.bihe.onlineShopping.entity.Rating;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.List;

public class RatingRepository {

    public static int findByPersonAndMerchendise(long personId,int merId){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "select r.star from Rating r inner join  Person p on p.id=r.person.id inner join Merchendise m on m.id=r.merchendise.id where p.id = :pId and m.id= :mId";

        Query query = session.createQuery(hql);
        query.setParameter("pId",(int) personId);
        query.setParameter("mId",merId);
        query.setMaxResults(1);
        int result = (int) query.getSingleResult();



        session.close();

        return result;
    }

    public static Rating findObjectByPersonAndMerchendise(long personId,int merId){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from Rating r where person.id = :pId and merchendise.id= :mId";

        Query query = session.createQuery(hql);
        query.setParameter("pId",(int) personId);
        query.setParameter("mId",merId);
        query.setMaxResults(1);
        Rating result =null;
        try {
            result = (Rating) query.getSingleResult();
        }catch (Exception e){

        }
        session.close();

        return result;
    }
}
