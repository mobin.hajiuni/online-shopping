package org.bihe.onlineShopping.repository;

import org.bihe.onlineShopping.entity.Category;
import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.enums.Role;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import javax.persistence.Query;
import java.util.List;

public class CategoryRepository {
    public static List findAll() {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from Category";

        Query query = session.createQuery(hql);
        List<Category> result = query.getResultList();

        for (Category category:result) {

        }

        session.close();

        return result;
    }

    public static Category findById(int id) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from Category where id = :id";

        Query query = session.createQuery(hql);
        query.setParameter("id",id);
        Category result = (Category) query.getSingleResult();

        session.close();

        return result;
    }

    public static List findCount(int categoryId){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();
        String hql = "select count(*) from Merchendise m join m.categories c where c.id = :id";
        Query query = session.createQuery(hql);
        query.setParameter("id",categoryId);
        List result =  query.getResultList();
        session.close();
        return result;
    }

        public static List findCategory(String name) {
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            Session session = configuration.buildSessionFactory().openSession();

            String hql = "from Category where name like :namei";
            Query query = session.createQuery(hql);
            query.setParameter("namei",name);
            List result = query.getResultList();
            session.close();
            return result;
        }

        public static void addCategory(String category) {
            Category cat = new Category();
            cat.setName(category);
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            Session session = configuration.buildSessionFactory().openSession();

            Transaction transaction = session.beginTransaction();
            session.persist(cat);
            transaction.commit();
            session.close();
        }
        public static List readCategories() {
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            Session session = configuration.buildSessionFactory().openSession();

            String hql = "from Category";
            Query query = session.createQuery(hql);
            List result = query.getResultList();
            session.close();
            return result;
        }
}
