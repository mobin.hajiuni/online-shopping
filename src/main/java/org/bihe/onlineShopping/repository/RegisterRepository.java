package org.bihe.onlineShopping.repository;

import org.bihe.onlineShopping.entity.Order;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import javax.persistence.Parameter;
import javax.persistence.Query;
import java.util.List;

public class RegisterRepository {
    public static boolean RegisterCheck( String userName){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql1 = "from Person p where p.userName=:uname";
        Query query1 = session.createQuery(hql1);
        query1.setParameter("uname", userName);
        List name = query1.getResultList();

        session.close();
        if( name.size()>0){
            return true;
        }
        else{
            return false;
        }

    }
}
