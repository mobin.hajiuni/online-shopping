package org.bihe.onlineShopping.repository;

import org.bihe.onlineShopping.entity.*;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

public class BasketRepository {
    public static Basket basketStatus(int id, boolean status){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from Basket b where b.person.id=:idd and b.status =:statuss";
        Query query = session.createQuery(hql);
        query.setParameter("idd", id);
        query.setParameter("statuss", status);
        List<Basket> result = query.getResultList();
        Basket basket = null;
        try {
             basket = result.get(0);

        }
        catch (Exception e){

        }


        session.close();

        return basket;
    }
    public static List<BasketLine> findAllTheBaskets(int id){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "select bl from BasketLine bl inner join Merchendise m on bl.merchendise.id= m.id where m.person.id =: idd";
        Query query = session.createQuery(hql);
        query.setParameter("idd", id);
        List<BasketLine> basketLines = query.getResultList();

        session.close();

        return basketLines;

    }
    public static List<BasketLine> findByDate(LocalDateTime dat1,LocalDateTime dat2, int id){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();
        Instant instant1 = dat1.toInstant(ZoneOffset.UTC);
        Date date1 = Date.from(instant1);
        Instant instant2 = dat2.toInstant(ZoneOffset.UTC);
        Date date2 = Date.from(instant2);
        if(date1.before(date2)){
            String hql = "select bl from BasketLine bl inner join Merchendise m on bl.merchendise.id= m.id Where bl.createdAt BETWEEN :Date1 AND :Date2 And m.person.id=:idd";
            Query query = session.createQuery(hql);
            query.setParameter("Date1",dat1);
            query.setParameter("Date2",dat2);
            query.setParameter("idd", id);
            List<BasketLine> result = query.getResultList();

            session.close();
            return result;
        }
        else{
            String hql = "select bl from BasketLine bl inner join Merchendise m on bl.merchendise.id= m.id Where bl.createdAt BETWEEN :Date1 AND :Date2 And m.person.id=:idd";
            Query query = session.createQuery(hql);
            query.setParameter("Date1",dat2);
            query.setParameter("Date2",dat1);
            query.setParameter("idd", id);
            List<BasketLine> result = query.getResultList();
            session.close();
            return result;
        }
    }

}
