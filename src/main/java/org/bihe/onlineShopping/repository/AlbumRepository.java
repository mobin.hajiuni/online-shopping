package org.bihe.onlineShopping.repository;

import org.bihe.onlineShopping.entity.Album;
import org.bihe.onlineShopping.entity.Category;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;

public class AlbumRepository {
    public static Album findById(int id) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from Album where id = :id";

        Query query = session.createQuery(hql);
        query.setParameter("id",id);
        Album result = (Album) query.getSingleResult();

        session.close();

        return result;
    }
}
