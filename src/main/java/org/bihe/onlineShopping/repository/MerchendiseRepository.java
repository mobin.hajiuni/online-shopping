package org.bihe.onlineShopping.repository;

import org.bihe.onlineShopping.entity.Album;
import org.bihe.onlineShopping.entity.Merchendise;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.List;

public class MerchendiseRepository {

    public static List<Merchendise> findSelectedItems() {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from Merchendise where selectedItems = true";

        Query query = session.createQuery(hql);
        List<Merchendise> result = (List<Merchendise>) query.getResultList();

        if (result.size()>0){
            for (Merchendise mer:result) {
                List<Album> albums =  mer.getAlbum();
                String cat = mer.getCategories().get(0).getName();
                if (albums.size()>0){
                    String img = albums.get(0).getImage();
                }
            }
        }

        session.close();

        return result;
    }

    public static List<Merchendise> findAll(){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from Merchendise where quantity > 0";

        Query query = session.createQuery(hql);
        List<Merchendise> result = (List<Merchendise>) query.getResultList();

        if (result.size()>0){
            for (Merchendise mer:result) {
                List<Album> albums =  mer.getAlbum();
                String cat = mer.getCategories().get(0).getName();
                if (albums.size()>0){
                    String img = albums.get(0).getImage();
                }
            }
        }

        session.close();

        return result;
    }

    public static List<Merchendise> findByCategoryId(int inputId){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "select merchendise from Category c where c.id=:id";

        Query query = session.createQuery(hql);
        query.setParameter("id",inputId);
        query.setFirstResult(0);
        query.setMaxResults(9);
        List<Merchendise> result =(List<Merchendise>) query.getResultList();

        if (result.size()>0){
            for (Merchendise mer:result) {
                List<Album> albums =  mer.getAlbum();
                String cat = mer.getCategories().get(0).getName();
                if (albums.size()>0){
                    String img = albums.get(0).getImage();
                }
            }
        }

        session.close();

        return result;
    }

    public static List<Merchendise> findByCategoryAndproductName(String category,String title){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "select m from Category c join c.merchendise m where c.name like :cat and (m.title like :tit or m.brand like :tit or m.summary like :tit) and m.quantity >0";

        Query query = session.createQuery(hql);
        query.setParameter("cat",category);
        query.setParameter("tit",title);
        List<Merchendise> result =(List<Merchendise>) query.getResultList();

        if (result.size()>0){
            for (Merchendise mer:result) {
                List<Album> albums =  mer.getAlbum();
                String cat = mer.getCategories().get(0).getName();
                if (albums.size()>0){
                    String img = albums.get(0).getImage();
                }
            }
        }

        session.close();

        return result;
    }

    public static List<Merchendise> findByproductName(String title){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from  Merchendise m where (m.title like :tit or m.brand like :tit or m.summary like :tit) and m.quantity > 0";

        Query query = session.createQuery(hql);
        query.setParameter("tit",title);
        List<Merchendise> result =(List<Merchendise>) query.getResultList();

        if (result.size()>0){
            for (Merchendise mer:result) {
                List<Album> albums =  mer.getAlbum();
                String cat = mer.getCategories().get(0).getName();
                if (albums.size()>0){
                    String img = albums.get(0).getImage();
                }
            }
        }

        session.close();

        return result;
    }

    public static List<Merchendise> findByCategory(String category){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "select m from Category c join c.merchendise m where c.name like :cat and m.quantity > 0";

        Query query = session.createQuery(hql);
        query.setParameter("cat",category);
        List<Merchendise> result =(List<Merchendise>) query.getResultList();

        if (result.size()>0){
            for (Merchendise mer:result) {
                List<Album> albums =  mer.getAlbum();
                String cat = mer.getCategories().get(0).getName();
                if (albums.size()>0){
                    String img = albums.get(0).getImage();
                }
            }
        }

        session.close();

        return result;
    }

    public static Merchendise findById(int id){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        String hql = "from Merchendise where id = :idd";

        Query query = session.createQuery(hql);
        query.setParameter("idd",id);

        if (hql.equals("")){
            System.out.println();
        }

        Merchendise result =(Merchendise) query.getSingleResult();



        session.close();

        return result;
    }
}
