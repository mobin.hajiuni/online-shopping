package org.bihe.onlineShopping.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @Basic(fetch = FetchType.EAGER)
    private int id;

    @Column()
    private int star;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id",name = "merchendise_id", nullable = true)
    private Merchendise merchendise;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id",name = "person_id", nullable = false)
    private Person person;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    //---------constructor---------
    public Rating() {

    }
    //----------getters and setters--------

    public int getId() {
        return id;
    }

    public void setId(int rateId) {
        this.id = rateId;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public Merchendise getMerchendise() {
        return merchendise;
    }

    public void setMerchendise(Merchendise merchendise) {
        this.merchendise = merchendise;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}

