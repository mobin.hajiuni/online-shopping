package org.bihe.onlineShopping.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "basket_line")
public class BasketLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false,nullable = false)
    @Basic(fetch = FetchType.EAGER)
    private int id;

    @ManyToOne()
    @JoinColumn(referencedColumnName = "id",name = "merchendise_id",nullable = false)
    private Merchendise merchendise;

    @Column(name = "quantity",nullable = false)
    private int quantity = 1;

    @ManyToOne()
    @JoinColumn(referencedColumnName = "id",name = "basket_id",nullable = false)
    private Basket basket;

    @Column(name = "unit_price")
    private double unitPrice;

    @Column(name = "toal_price")
    private double totalPrice;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    //-------------constructor---------------

    public BasketLine() {
    }

    //------------Getter and Setters---------

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Merchendise getMerchendise() {
        return merchendise;
    }

    public void setMerchendise(Merchendise merchendise) {
        this.merchendise = merchendise;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
