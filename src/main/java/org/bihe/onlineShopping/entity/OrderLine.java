package org.bihe.onlineShopping.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
public class OrderLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @Basic(fetch = FetchType.EAGER)
    private int id;

    @Column()
    private double totalPrice;

    @Column()
    private double unitPrice;

    @Column()
    private int quantity;

    @ManyToOne()
    @JoinColumn(referencedColumnName = "id",name = "order_id", nullable = false)
    private Order order;

    @ManyToOne()
    @JoinColumn(referencedColumnName = "id",name = "merchendise_id", nullable = false)
    private Merchendise merchendise;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

//---------------constructor----------------
    public OrderLine() {

    }
//---------------getters and setters----------

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Merchendise getMerchendise() {
        return merchendise;
    }

    public void setMerchendise(Merchendise merchendise) {
        this.merchendise = merchendise;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}

