package org.bihe.onlineShopping.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @Basic(fetch = FetchType.EAGER)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "id", name = "merchendise_id", nullable = false)
    private Merchendise merchendise;

    @Column(name = "picture_address")
    private String picAddress;

    @Column(name = "image", nullable = true)
    @Type(type = "text")
    private String image;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    //----------------constructor------------
    public Album() {

    }
    //--------------getters and setters----------


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Merchendise getMerchendise() {
        return merchendise;
    }

    public void setMerchendise(Merchendise merchendises) {
        this.merchendise = merchendises;
    }

    public String getPicAddress() {
        return picAddress;
    }

    public void setPicAddress(String picAddress) {
        this.picAddress = picAddress;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}

