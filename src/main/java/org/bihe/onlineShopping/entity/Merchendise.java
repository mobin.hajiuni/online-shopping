package org.bihe.onlineShopping.entity;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table
public class Merchendise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @Basic(fetch = FetchType.EAGER)
    private int id;

    @Column(nullable = false)
    private String title;

    @Column(columnDefinition = "varchar(max)")
    @Type(type = "text")
    private String summary;

    @Column()
    private double price;

    @Column()
    private int quantity;

    @Column()
    private int avgRating = 5;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "Mer_Cat",
            joinColumns = {@JoinColumn(name = "merchendise_id")},
            inverseJoinColumns = {@JoinColumn(name = "category_id")}
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Category> categories = new ArrayList<>();

    @Column()
    private String color;

    @Column()
    private String brand;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id",name = "person_id", nullable = false)
    private Person person;

    @OneToMany(mappedBy = "merchendise")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Album> album = new ArrayList<>();

    @OneToMany(mappedBy = "merchendise")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Rating> ratings = new ArrayList<>();

    @OneToMany(mappedBy = "merchendise")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Comment> comments = new ArrayList<>();

    @OneToMany(mappedBy = "merchendise")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<BasketLine> basketLines = new ArrayList<>();

    @Column(name = "selected_items",nullable = true)
    private boolean selectedItems =false;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    //----------------constructor-------------

    public Merchendise() {
    }

    //----------------getter and setters----------


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(int avgRating) {
        this.avgRating = avgRating;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<Album> getAlbum() {
        return album;
    }

    public void setAlbum(List<Album> album) {
        this.album = album;
    }

    public void addAlbum(Album album) {
        this.album.add(album);
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public void addRating(Rating rating) {
        this.ratings.add(rating);
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
    }

    public List<BasketLine> getBasketLines() {
        return basketLines;
    }

    public void setBasketLines(List<BasketLine> basketLines) {
        this.basketLines = basketLines;
    }

    public boolean isSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(boolean selectedItems) {
        this.selectedItems = selectedItems;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}

