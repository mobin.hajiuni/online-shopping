package org.bihe.onlineShopping.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @Basic(fetch = FetchType.EAGER)
    private int id;

    @Column(name = "name",nullable = false)
    private String name;

    @ManyToMany(mappedBy = "categories")
    private List<Merchendise> merchendise = new ArrayList<>();

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    //-----------constructor----------
    public Category() {
    }

    //-----------getters and setters------

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Merchendise> getMerchendise() {
        return merchendise;
    }

    public void setMerchendise(List<Merchendise> merchendise) {
        this.merchendise = merchendise;
    }

    public void addMerchendise(Merchendise merchendise) {
        this.merchendise.add(merchendise);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
