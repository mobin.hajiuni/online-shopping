package org.bihe.onlineShopping.servlet;


import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.repository.PersonRepository;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.bihe.onlineShopping.repository.PersonRepository;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet(urlPatterns = "/login")
public class LogIn extends HttpServlet {
    private static boolean log = false;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/Login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String pass = req.getParameter("password");
        HttpSession sessions = req.getSession();


        if (username != null && pass != null && username != "" && pass != "") {
            List<Person> result = PersonRepository.findByUserName(username);

            //if found sb
            if (result != null) {

                for(int i=0;i<result.size();i++){
                    if( result.get(i).getPassword().equals(pass)){
                        //login success
                        log = true;
                        Person p = (Person) result.get(0);
                        sessions.setAttribute("OnlineUser", result.get(0));
                        sessions.setAttribute("IsLoggedIn", result.get(0));
                        req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/index.jsp").forward(req, resp);
                        break;
                    }
                }

            }
            if(!log){
                //or error page
                System.out.println("wrong input");
                resp.sendRedirect("login");
            }
        } else {
            System.out.println("nothing found!");
            resp.sendRedirect("login");
        }

    }
}