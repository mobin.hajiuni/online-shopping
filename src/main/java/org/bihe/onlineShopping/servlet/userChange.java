package org.bihe.onlineShopping.servlet;
import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.repository.Insert;
import org.bihe.onlineShopping.repository.PersonRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/userChange")
public class userChange extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String op = req.getParameter("op");
        List<Person> result = PersonRepository.findByID(Integer.parseInt(id));

        if(op.equals("edit")){
            //show new page
            req.setAttribute("editUserID",result);
            req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/ShowSingleUser.jsp").forward(req, resp);

        }else if(op.equals("deny")){
            //delete user
            result.get(0).setDeleted(true);
            Insert.updateData(result.get(0));

        }
        resp.sendRedirect("users");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = (long) req.getSession().getAttribute("idU");
        String op = (String) req.getSession().getAttribute("opU");

        String roll =req.getParameter("roll");
        List<Person> result = PersonRepository.findByID((int) id);
        if(op.equals("save")){
            //edit roll of user
            if(Integer.parseInt(roll) == 1){
                result.get(0).setRole("seller");
            }else if(Integer.parseInt(roll) == 2){
                result.get(0).setRole("user");
            }
            Insert.updateData(result.get(0));
        }
        resp.sendRedirect("users");

    }

}