package org.bihe.onlineShopping.servlet;

import org.bihe.onlineShopping.entity.Merchendise;
import org.bihe.onlineShopping.repository.MerchendiseRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SingleProductPageServlet", urlPatterns = "/single-product-page")
public class SingleProductPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String stringId = request.getParameter("id");
        int id = Integer.parseInt(stringId);

        Merchendise merchendise = MerchendiseRepository.findById(id);

        request.setAttribute("merchendise",merchendise);

        request.getRequestDispatcher("/org.bihe.onlineShopping.jsp/singlePageProduct.jsp").forward(request, response);

    }
}
