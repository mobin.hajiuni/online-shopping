package org.bihe.onlineShopping.servlet;

import org.bihe.onlineShopping.entity.Comment;
import org.bihe.onlineShopping.entity.Merchendise;
import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.entity.Rating;
import org.bihe.onlineShopping.repository.Insert;
import org.bihe.onlineShopping.repository.MerchendiseRepository;
import org.bihe.onlineShopping.repository.PersonRepository;
import org.bihe.onlineShopping.repository.RatingRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "SaveCommentServlet", urlPatterns = "/save-comment")
public class SaveCommentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String message = request.getParameter("comment");
        String rate = request.getParameter("stars");
        String merId = request.getParameter("merchendiseId");
        String logedInPerson = request.getParameter("personId");

        try{
            Person person = PersonRepository.findByUserId(Integer.parseInt(logedInPerson));
            Merchendise merchendise = MerchendiseRepository.findById(Integer.parseInt(merId));
            Rating rating = RatingRepository.findObjectByPersonAndMerchendise(Long.parseLong(logedInPerson), Integer.parseInt(merId));

            if (message != null && message != "") {
                Comment comment = new Comment();
                comment.setComment(message);
                comment.setMerchendise(merchendise);
                comment.setPerson(person);
                Insert.insertData(comment);
            }

            if (Integer.parseInt(rate) != 0) {
                if (rating != null) {
                    rating.setStar(Integer.parseInt(rate));
                    Insert.updateData(rating);
                } else {
                    Rating newRating = new Rating();
                    newRating.setMerchendise(merchendise);
                    newRating.setPerson(person);
                    newRating.setStar(Integer.parseInt(rate));
                    Insert.insertData(newRating);
                }

            }
            response.setContentType("text/plain");
            PrintWriter out = response.getWriter();
            out.write("Success");
            out.close();
        }catch (Exception e){
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write("Faild");
            out.close();
        }



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
