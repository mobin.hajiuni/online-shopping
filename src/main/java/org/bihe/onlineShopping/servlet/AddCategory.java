package org.bihe.onlineShopping.servlet;
import org.bihe.onlineShopping.entity.Category;
import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.repository.CategoryRepository;
import org.bihe.onlineShopping.repository.Insert;
import org.bihe.onlineShopping.repository.PersonRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet("/addCategory")
public class AddCategory extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //read all categories
        List<Category> result = CategoryRepository.readCategories();
        req.setAttribute("categoryName",result);
        req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/AddCategory.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String category = req.getParameter("cat");

        if(category != ""){
            List<Category> result = CategoryRepository.findCategory(category);
            //if it is not repetitious, then add it to database
            if (result.size() == 0) {
                Category cat = new Category();
                cat.setName(category);
                Insert.insertData(cat);
            }
        }
        resp.sendRedirect("addCategory");
    }
}
