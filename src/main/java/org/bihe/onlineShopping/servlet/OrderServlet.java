package org.bihe.onlineShopping.servlet;

import org.bihe.onlineShopping.entity.*;
import org.bihe.onlineShopping.entity.Basket;
import org.bihe.onlineShopping.repository.BasketRepository;
import org.bihe.onlineShopping.repository.Insert;
import org.bihe.onlineShopping.repository.OrderRepository;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet("/Order")
public class OrderServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request , HttpServletResponse response) throws ServletException, IOException {


        Order order = (Order)request.getSession().getAttribute("order");
        Person person = order.getPerson();
        List<OrderLine> orderLines = order.getOrderLine();

        for(OrderLine OLdec : orderLines){
            Merchendise merchendise = OLdec.getMerchendise();
            int q = merchendise.getQuantity();
            merchendise.setQuantity(q-OLdec.getQuantity());
            Insert.updateData(merchendise);
        }

        Basket basket = BasketRepository.basketStatus((int) person.getId(), true);
        if(basket == null) {
            Basket basket1 = new Basket();
            basket1.setStatus(true);
            basket1.setPerson(person);
            //List<BasketLine> basketLines = new ArrayList<>();
            Insert.insertData(basket1);
            for (OrderLine orderLine : orderLines) {
                BasketLine basketLine = new BasketLine();
                basketLine.setQuantity(orderLine.getQuantity());
                basketLine.setMerchendise(orderLine.getMerchendise());
                basketLine.setUnitPrice(orderLine.getUnitPrice());
                basketLine.setTotalPrice(orderLine.getTotalPrice());
                basket1.setStatus(false);
                basketLine.setBasket(basket1);
                Insert.insertData(basketLine);

            }

        }
        else{
            for (OrderLine orderLine : orderLines) {
                BasketLine basketLine = new BasketLine();
                basketLine.setQuantity(orderLine.getQuantity());
                basketLine.setMerchendise(orderLine.getMerchendise());
                basketLine.setUnitPrice(orderLine.getUnitPrice());
                basketLine.setTotalPrice(orderLine.getTotalPrice());
                basketLine.setBasket(basket);
                basket.setStatus(false);
                Insert.insertData(basketLine);
            }

        }
        order.setStatus(false);
        Insert.updateData(order);

        RequestDispatcher req = request.getRequestDispatcher("org.bihe.onlineShopping.jsp/index.jsp");
        req.forward(request, response);
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        session.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        Person logenInPerson = (Person) request.getSession().getAttribute("IsLoggedIn");
        if (logenInPerson == null) {
            RequestDispatcher req = request.getRequestDispatcher("org.bihe.onlineShopping.jsp/Login.jsp");
            req.forward(request, resp);
        } else {
            int id = (int) logenInPerson.getId();
            List<Order> order = OrderRepository.findActiveOrders(id);

            if (order.size()<1) {
                RequestDispatcher req = request.getRequestDispatcher("org.bihe.onlineShopping.jsp/index.jsp");
                req.forward(request, resp);
            }else if(!order.get(0).isStatus()){
                RequestDispatcher req = request.getRequestDispatcher("org.bihe.onlineShopping.jsp/index.jsp");
                req.forward(request, resp);
            }

            request.setAttribute("order", order.get(0));
            request.getRequestDispatcher("/org.bihe.onlineShopping.jsp/order.jsp").forward(request, resp);
        }
    }
}
