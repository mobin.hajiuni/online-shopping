package org.bihe.onlineShopping.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SignOutServlet",urlPatterns = "/sign-out")
public class SignOutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("IsLoggedIn",null);
        request.getSession().setAttribute("OnlineUser",null);

        request.getRequestDispatcher("/org.bihe.onlineShopping.jsp/index.jsp").forward(request, response);

    }
}
