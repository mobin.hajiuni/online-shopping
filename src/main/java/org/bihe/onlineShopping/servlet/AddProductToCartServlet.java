package org.bihe.onlineShopping.servlet;

import org.bihe.onlineShopping.entity.Merchendise;
import org.bihe.onlineShopping.entity.Order;
import org.bihe.onlineShopping.entity.OrderLine;
import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.repository.Insert;
import org.bihe.onlineShopping.repository.MerchendiseRepository;
import org.bihe.onlineShopping.repository.OrderRepository;
import org.bihe.onlineShopping.repository.PersonRepository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "AddProductToCartServlet",urlPatterns = "/add-to-cart")
public class AddProductToCartServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String personId = request.getParameter("personId");
        String merId = request.getParameter("merchendiseId");

        try {

            List<Person> person = PersonRepository.findByID(Integer.parseInt(personId));
            Merchendise merchendise = MerchendiseRepository.findById(Integer.parseInt(merId));

            List<Order> orders = OrderRepository.findActiveOrders(Integer.parseInt(personId));

            if (orders.size() > 0) {
                Order order = orders.get(0);

                OrderLine orderLine = new OrderLine();
                orderLine.setMerchendise(merchendise);
                orderLine.setOrder(order);
                orderLine.setQuantity(1);
                orderLine.setUnitPrice(merchendise.getPrice());
                orderLine.setTotalPrice(1 * merchendise.getPrice());

                Insert.insertData(orderLine);
            } else {
                Order order = new Order();
                order.setPerson(person.get(0));
                order.setStatus(true);

                OrderLine orderLine = new OrderLine();
                orderLine.setMerchendise(merchendise);
                orderLine.setOrder(order);
                orderLine.setQuantity(1);
                orderLine.setUnitPrice(merchendise.getPrice());
                orderLine.setTotalPrice(1 * merchendise.getPrice());

                Insert.insertData(order);
                Insert.insertData(orderLine);
            }

            response.setContentType("text/plain");
            PrintWriter out = response.getWriter();
            out.write("Success");
            out.close();
        }catch (Exception e){
            response.setContentType("text/plain");
            PrintWriter out = response.getWriter();
            out.write("Failed");
            out.close();
        }




    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
