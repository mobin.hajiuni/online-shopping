package org.bihe.onlineShopping.servlet;

import org.bihe.onlineShopping.entity.Category;
import org.bihe.onlineShopping.repository.CategoryRepository;
import org.bihe.onlineShopping.repository.PersonRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/sellerRequest")
public class SellerRequest extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //read all requests
        List<Category> result = PersonRepository.findByAcceptance(false);
        req.setAttribute("sellRequest",result);
        req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/SellerRequest.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}