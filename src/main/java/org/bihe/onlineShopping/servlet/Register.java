package org.bihe.onlineShopping.servlet;

import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.enums.Role;
import org.bihe.onlineShopping.repository.Insert;
import org.bihe.onlineShopping.repository.RegisterRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

@WebServlet("/Register")
public class Register extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("src/main/webapp/org.bihe.onlineShopping.jsp/index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String first_name = request.getParameter("firstname");
        String last_name = request.getParameter("lastname");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String checkBox = request.getParameter("conditions");

        boolean exist = RegisterRepository.RegisterCheck(username);


        if (!exist) {

            Person user = new Person();
            user.setFirstName(first_name);
            user.setLastName(last_name);
            user.setEmail(email);
            user.setUserName(username);
            user.setPassword(password);
            if (checkBox.equals("false")) {
                user.setRole(String.valueOf(Role.user));
                user.setAcceptance(true);
            } else {
                String companyName = request.getParameter("companyname");
                String address = request.getParameter("adress");
                String mobile = request.getParameter("mobilenumber");
                user.setRole(String.valueOf(Role.seller));
                user.setCompanyName(companyName);
                user.setCompanyAddress(address);
                user.setMobileNumber(mobile);
                user.setAcceptance(false);
            }


            try {
                Configuration configuration = new Configuration();
                configuration.configure("hibernate.cfg.xml");
                Session session = configuration.buildSessionFactory().openSession();

                Transaction transaction = session.beginTransaction();

                session.persist(user);

                transaction.commit();

                session.close();

//                request.getRequestDispatcher("org.bihe.onlineShopping.jsp/index.jsp");
                response.setContentType("text/plain");
                PrintWriter out = response.getWriter();
                out.write("Success");
                out.close();

            } catch (Exception e) {
                response.setContentType("text/plain");
                PrintWriter out = response.getWriter();
                out.write("Failed");
                out.close();
            }
        } else {
            response.setContentType("text/plain");
            PrintWriter out = response.getWriter();
            out.write("Failed");
            out.close();
        }

    }

}


