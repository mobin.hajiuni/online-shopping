package org.bihe.onlineShopping.servlet;

import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.repository.Insert;
import org.bihe.onlineShopping.repository.PersonRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/acceptance")
public class Acceptance extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        String op = req.getParameter("op");

        List<Person> result = PersonRepository.findByID(Integer.parseInt(id));
        if(op.equals("accept")){
            //add to seller
            result.get(0).setAcceptance(true);
            Insert.updateData(result.get(0));
        }else if(op.equals("deny")){
            //delete user
            result.get(0).setDeleted(true);
            Insert.updateData(result.get(0));
        }
        resp.sendRedirect("sellerRequest");
    }
}