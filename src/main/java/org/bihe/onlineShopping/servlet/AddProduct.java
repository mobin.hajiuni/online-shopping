package org.bihe.onlineShopping.servlet;

import org.bihe.onlineShopping.entity.Album;
import org.bihe.onlineShopping.entity.Category;
import org.bihe.onlineShopping.entity.Merchendise;
import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.repository.CategoryRepository;
import org.bihe.onlineShopping.repository.Insert;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@MultipartConfig
@WebServlet(name = "AddProduct", urlPatterns = "/add-product")
public class AddProduct extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Person person = (Person) session.getAttribute("IsLoggedIn");

        if (person != null) {
            String title = request.getParameter("title");
            String description = request.getParameter("description");
            String cat1 = request.getParameter("category1");
            String cat2 = request.getParameter("category2");
            String cat3 = request.getParameter("category3");
            String price = request.getParameter("price");
            String quantity = request.getParameter("quantity");
            String color = request.getParameter("color");
            String brand = request.getParameter("brand");

            Part filePart = request.getPart("file");
            InputStream fileContent = filePart.getInputStream();
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            String[] splitedFileName = fileName.split("\\.");
            String prefix = splitedFileName[splitedFileName.length - 1];
            File upload = new File(".");
            File file = File.createTempFile("somName-", "." + prefix, upload);
            Files.copy(fileContent, file.toPath(), StandardCopyOption.REPLACE_EXISTING);

            Album album = new Album();
            album.setPicAddress(delteLastChar(upload.getAbsolutePath()) + file.getName());
            List<Album> albums = new ArrayList<>();
            albums.add(album);

            //----------save pic------------------
            String base64;
            try {
                FileInputStream fileInputStreamReader = new FileInputStream(file);
                byte[] fileContent2 = new byte[(int) filePart.getSize()];

                fileInputStreamReader.read(fileContent2);
                base64 = Base64.getEncoder().encodeToString(fileContent2);

                album.setImage("data:image/jpeg;base64," + base64);
            }catch (FileNotFoundException e){

            }catch (IOException e){

            }


            //------------------------------------

            Merchendise merchendise = new Merchendise();
            merchendise.setTitle(title);
            merchendise.setSummary(description);
            List<Category> catrgories = new ArrayList<>();

            if (cat1 != null && !cat1.equals("")) {
                int id = Integer.parseInt(cat1);
                Category category = CategoryRepository.findById(id);
                catrgories.add(category);
            }
            if (cat2 != null && !cat2.equals("")) {
                int id = Integer.parseInt(cat2);
                Category category = CategoryRepository.findById(id);
                catrgories.add(category);
            }
            if (cat3 != null && !cat3.equals("")) {
                int id = Integer.parseInt(cat3);
                Category category = CategoryRepository.findById(id);
                catrgories.add(category);
            }
            merchendise.setPrice(Double.parseDouble(price));
            merchendise.setQuantity(Integer.parseInt(quantity));
            merchendise.setColor(color);
            merchendise.setBrand(brand);
            merchendise.setAlbum(albums);
            merchendise.setCategories(catrgories);

            merchendise.setPerson(person);



            album.setMerchendise(merchendise);

            Insert.insertData(merchendise);
            Insert.insertData(album);

            response.sendRedirect("org.bihe.onlineShopping.jsp/addProduct.jsp");
        }else {
            request.getRequestDispatcher("/org.bihe.onlineShopping.jsp/Login.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public String delteLastChar(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == '.') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }
}
