package org.bihe.onlineShopping.servlet;

import org.bihe.onlineShopping.entity.Category;
import org.bihe.onlineShopping.entity.Merchendise;
import org.bihe.onlineShopping.repository.CategoryRepository;
import org.bihe.onlineShopping.repository.MerchendiseRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/product-view")
public class ProductViewServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String value = req.getParameter("whichButton");

        List<Category> categories = CategoryRepository.findAll();
        req.setAttribute("cats", categories);

        if (value.equals("all")) {
            List<Merchendise> merchendises = MerchendiseRepository.findAll();

            req.setAttribute("merchendises", merchendises);

            req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/productPage.jsp").forward(req, resp);
        } else if (value.equals("search")) {
            String category = req.getParameter("categorySelect");
            String productName = req.getParameter("productName");

            if ((category != null) && (productName != null && !productName.equals("")) && (!category.equals("Select Category"))) {
                List<Merchendise> merchendises = MerchendiseRepository.findByCategoryAndproductName(category, productName);
                req.setAttribute("merchendises", merchendises);

                req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/productPage.jsp").forward(req, resp);

            } else if ((category.equals("Select Category") || category.equals("")) && (productName != null && !productName.equals(""))) {
                List<Merchendise> merchendises = MerchendiseRepository.findByproductName(productName);
                req.setAttribute("merchendises", merchendises);

                req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/productPage.jsp").forward(req, resp);

            } else if ((!category.equals("")) && (!category.equals("Select Category")) && (productName == null || productName.equals(""))) {
                List<Merchendise> merchendises = MerchendiseRepository.findByCategory(category);
                req.setAttribute("merchendises", merchendises);

                req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/productPage.jsp").forward(req, resp);

            } else {
                List<Merchendise> merchendises = MerchendiseRepository.findAll();

                req.setAttribute("merchendises", merchendises);

                req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/productPage.jsp").forward(req, resp);
            }
        } else {
            List<Merchendise> merchendises = MerchendiseRepository.findByCategoryId(Integer.parseInt(value));

            req.setAttribute("merchendises", merchendises);

            req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/productPage.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String category = req.getParameter("category");

        List<Merchendise> merchendises = null;
        if (category.equals("all")){
            merchendises = MerchendiseRepository.findAll();
        }else{
            merchendises = MerchendiseRepository.findByCategory(category);
        }
        List<Category> categories = CategoryRepository.findAll();


        req.setAttribute("merchendises", merchendises);
        req.setAttribute("cats", categories);


        req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/productPage.jsp").forward(req, resp);

    }
}
