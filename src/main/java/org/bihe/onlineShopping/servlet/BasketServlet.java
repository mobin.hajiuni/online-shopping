package org.bihe.onlineShopping.servlet;

import org.bihe.onlineShopping.entity.Basket;
import org.bihe.onlineShopping.entity.BasketLine;
import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.repository.BasketRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@WebServlet("/Basket")
public class BasketServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Person logenInPerson = (Person) req.getSession().getAttribute("IsLoggedIn");
        int id = (int) logenInPerson.getId();
        //int id = (int) req.getSession().getAttribute("id");
        String d1 = req.getParameter("date1");
        String d2 = req.getParameter("date2");

        if(d1 != null) {

            d1 = d1 + " 00:00:00";


            d2 = d2 + " 00:00:00";
        }


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime1 = LocalDateTime.parse(d1,formatter);
        LocalDateTime dateTime2 = LocalDateTime.parse(d2,formatter);



        if(d1 == null || d2 == null){
            List<BasketLine> basketLines = BasketRepository.findAllTheBaskets(id);
            req.setAttribute("basket",basketLines);
            req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/basket.jsp").forward(req,resp);
        }
        else {


            List<BasketLine> basketLines = BasketRepository.findByDate(dateTime1,dateTime2,id);
            req.setAttribute("basket", basketLines);
            req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/basket.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Person logenInPerson = (Person) req.getSession().getAttribute("IsLoggedIn");
        if (logenInPerson == null) {
            RequestDispatcher request = req.getRequestDispatcher("org.bihe.onlineShopping.jsp/Login.jsp");
            request.forward(req, resp);
        } else {
            int id = (int) logenInPerson.getId();

            String d1 = req.getParameter("date1");
            String d2 = req.getParameter("date2");

            LocalDateTime dateTime1 = null;
            LocalDateTime dateTime2 = null;

            if (d1 != null) {

                d1 = d1 + " 00:00:00";


                d2 = d2 + " 00:00:00";
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                dateTime1 = LocalDateTime.parse(d1, formatter);
                dateTime2 = LocalDateTime.parse(d1, formatter);
            }


            if (d1 == null || d2 == null) {
                List<BasketLine> basketLines = BasketRepository.findAllTheBaskets(id);
                req.setAttribute("basket", basketLines);
                req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/basket.jsp").forward(req, resp);

            } else {


                List<BasketLine> basketLines = BasketRepository.findByDate(dateTime1, dateTime2, id);
                req.setAttribute("baskets", basketLines);
                req.getRequestDispatcher("/org.bihe.onlineShopping.jsp/basket.jsp").forward(req, resp);
            }
        }
    }
}
