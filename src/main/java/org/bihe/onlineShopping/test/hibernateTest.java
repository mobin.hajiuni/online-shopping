package org.bihe.onlineShopping.test;

import org.bihe.onlineShopping.entity.Person;
import org.bihe.onlineShopping.enums.Role;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class hibernateTest {
    private EntityManagerFactory entityManagerFactory;

    public static void main(String[] args) {
        Person person = new Person();
        person.setFirstName("Moein");
        person.setLastName("Hajiuni");
        person.setAcceptance(true);
        person.setEmail("mobin.hajiuni@gmail.com");
        person.setMobileNumber("09361660911");
        person.setPassword("12356");
        person.setUserName("mobin.hajiuni");
        person.setRole(Role.admin.toString());

        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        Session session = configuration.buildSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        session.persist(person);

        transaction.commit();

        session.close();

    }



}
